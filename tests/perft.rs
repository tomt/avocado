use avocado::{
    engine::perft,
    game::{board::Board, move_gen::MoveGen},
};
use lazy_static::lazy_static;

lazy_static! {
    static ref GEN: MoveGen = MoveGen::default();
}

macro_rules! perft {
    ($fen:tt, $nodes:tt, $depth:tt) => {
        let mut board = Board::new($fen).unwrap();
        let node_count = perft::perft($depth, &mut board, &GEN, None);
        assert_eq!(node_count, $nodes, "mismatched node count: {}", $fen);
    };
}

#[test]
fn depth_1() {
    perft!(
        "rnb2k1r/pp1Pbppp/2p5/q7/2B5/8/PPPQNnPP/RNB1K2R w QK - 3 9",
        39,
        1
    );
    perft!("r6r/1b2k1bq/8/8/7B/8/8/R3K2R b QK - 3 2", 8, 1);
    perft!("8/8/8/2k5/2pP4/8/B7/4K3 b - d3 5 3", 8, 1);
    perft!(
        "r1bqkbnr/pppppppp/n7/8/8/P7/1PPPPPPP/RNBQKBNR w QqKk - 2 2",
        19,
        1
    );
    perft!(
        "r3k2r/p1pp1pb1/bn2Qnp1/2qPN3/1p2P3/2N5/PPPBBPPP/R3K2R b QqKk - 3 2",
        5,
        1
    );
    perft!("2r5/3pk3/8/2P5/8/2K5/8/8 w - - 5 4", 9, 1);
    perft!(
        "2kr3r/p1ppqpb1/bn2Qnp1/3PN3/1p2P3/2N5/PPPBBPPP/R3K2R b QK - 3 2",
        44,
        1
    );
    perft!("4k3/8/8/5R2/8/8/8/4K3 b - - 0 1", 3, 1);
    perft!("8/4k3/8/8/4R3/8/8/4K3 b - - 0 1", 6, 1);
    perft!("4k3/6N1/5b2/4R3/8/8/8/4K3 b - - 0 1", 4, 1);
    perft!("4k3/8/6n1/4R3/8/8/8/4K3 b - - 0 1", 6, 1);
    perft!("8/8/8/2k5/3Pp3/8/8/4K3 b - d3 0 1", 9, 1);
    perft!("8/8/8/1k6/3Pp3/8/8/4KQ2 b - d3 0 1", 6, 1);
    perft!("4k3/8/4r3/8/8/4Q3/8/2K5 b - - 0 1", 9, 1);
    perft!("8/8/8/8/k2Pp2Q/8/8/2K5 b - d3 0 1", 6, 1);
    perft!(
        "rnbq1k1r/pp1Pbppp/2p5/8/2B5/8/PPP1NnPP/RNBQK2R w KQ - 1 8",
        44,
        1
    );
    perft!(
        "r4rk1/1pp1qppp/p1np1n2/2b1p1B1/2B1P1b1/P1NP1N2/1PP1QPPP/R4RK1 w - - 0 10",
        46,
        1
    );
    perft!(
        "r3k2r/Pppp1ppp/1b3nbN/nP6/BBP1P3/q4N2/Pp1P2PP/R2Q1RK1 w kq - 0 1",
        6,
        1
    );
    perft!(
        "r2q1rk1/pP1p2pp/Q4n2/bbp1p3/Np6/1B3NBn/pPPP1PPP/R3K2R b KQ - 0 1",
        6,
        1
    );
    perft!("8/2p5/3p4/KP5r/1R3p1k/8/4P1P1/8 w - - 0 1", 14, 1);
    perft!(
        "r3k2r/p1ppqpb1/bn2pnp1/3PN3/1p2P3/2N2Q1p/PPPBBPPP/R3K2R w KQkq - 0 1",
        48,
        1
    );
    perft!(
        "rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1",
        20,
        1
    );
}

#[test]
fn depth_2() {
    perft!(
        "rnbq1k1r/pp1Pbppp/2p5/8/2B5/8/PPP1NnPP/RNBQK2R w KQ - 1 8",
        1486,
        2
    );
    perft!(
        "r4rk1/1pp1qppp/p1np1n2/2b1p1B1/2B1P1b1/P1NP1N2/1PP1QPPP/R4RK1 w - - 0 10",
        2079,
        2
    );
    perft!(
        "r3k2r/Pppp1ppp/1b3nbN/nP6/BBP1P3/q4N2/Pp1P2PP/R2Q1RK1 w kq - 0 1",
        264,
        2
    );
    perft!(
        "r2q1rk1/pP1p2pp/Q4n2/bbp1p3/Np6/1B3NBn/pPPP1PPP/R3K2R b KQ - 0 1",
        264,
        2
    );
    perft!("8/2p5/3p4/KP5r/1R3p1k/8/4P1P1/8 w - - 0 1", 191, 2);
    perft!(
        "r3k2r/p1ppqpb1/bn2pnp1/3PN3/1p2P3/2N2Q1p/PPPBBPPP/R3K2R w KQkq - 0 1",
        2039,
        2
    );
    perft!(
        "rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1",
        400,
        2
    );
}

#[test]
fn depth_3() {
    perft!("8/2p5/3p4/KP5r/1R3p1k/8/4P1P1/8 w - - 0 1", 2812, 3);
    perft!(
        "r4rk1/1pp1qppp/p1np1n2/2b1p1B1/2B1P1b1/P1NP1N2/1PP1QPPP/R4RK1 w - - 0 10",
        89890,
        3
    );
    perft!(
        "r3k2r/Pppp1ppp/1b3nbN/nP6/BBP1P3/q4N2/Pp1P2PP/R2Q1RK1 w kq - 0 1",
        9467,
        3
    );
    perft!(
        "r2q1rk1/pP1p2pp/Q4n2/bbp1p3/Np6/1B3NBn/pPPP1PPP/R3K2R b KQ - 0 1",
        9467,
        3
    );
    perft!(
        "rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1",
        8902,
        3
    );
    perft!(
        "rnbq1k1r/pp1Pbppp/2p5/8/2B5/8/PPP1NnPP/RNBQK2R w KQ - 1 8",
        62379,
        3
    );
    perft!(
        "r3k2r/p1ppqpb1/bn2pnp1/3PN3/1p2P3/2N2Q1p/PPPBBPPP/R3K2R w KQkq - 0 1",
        97862,
        3
    );
}

#[test]
fn depth_4() {
    perft!("r3k2r/1b4bq/8/8/8/8/7B/R3K2R w KQkq - 0 1", 1274206, 4);
    perft!("r3k2r/8/3Q4/8/8/5q2/8/R3K2R b KQkq - 0 1", 1720476, 4);
    perft!("8/8/2k5/5q2/5n2/8/5K2/8 b - - 0 1", 23527, 4);
    perft!(
        "r3k2r/Pppp1ppp/1b3nbN/nP6/BBP1P3/q4N2/Pp1P2PP/R2Q1RK1 w kq - 0 1",
        422333,
        4
    );
    perft!(
        "r2q1rk1/pP1p2pp/Q4n2/bbp1p3/Np6/1B3NBn/pPPP1PPP/R3K2R b KQ - 0 1",
        422333,
        4
    );
    perft!("8/2p5/3p4/KP5r/1R3p1k/8/4P1P1/8 w - - 0 1", 43238, 4);
    perft!(
        "r3k2r/p1ppqpb1/bn2pnp1/3PN3/1p2P3/2N2Q1p/PPPBBPPP/R3K2R w KQkq - 0 1",
        4085603,
        4
    );
    perft!(
        "rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1",
        197281,
        4
    );
    perft!(
        "r3k2r/p1ppqpb1/bn1Ppnp1/4N3/1p2P3/2N2Q1p/PPPBBPPP/R3K2R b KQkq - 0 1",
        3835265,
        4
    );
}

#[test]
fn depth_5() {
    perft!(
        "r3k2r/Pppp1ppp/1b3nbN/nP6/BBP1P3/q4N2/Pp1P2PP/R2Q1RK1 w kq - 0 1",
        15833292,
        5
    );
    perft!("8/2p5/3p4/KP5r/1R3p1k/8/4P1P1/8 w - - 0 1", 674624, 5);
    perft!("8/8/1P2K3/8/2n5/1q6/8/5k2 b - - 0 1", 1004658, 5);
    perft!(
        "rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1",
        4865609,
        5
    );
    perft!(
        "r2q1rk1/pP1p2pp/Q4n2/bbp1p3/Np6/1B3NBn/pPPP1PPP/R3K2R b KQ - 0 1",
        15833292,
        5
    );
    perft!(
        "r3k2r/p1ppqpb1/bn2pnp1/3PN3/1p2P3/2N2Q1p/PPPBBPPP/R3K2R w KQkq - 0 1",
        193690690,
        5
    );
}

#[test]
fn depth_6() {
    perft!("3k4/3p4/8/K1P4r/8/8/8/8 b - - 0 1", 1134888, 6);
    perft!("8/8/4k3/8/2p5/8/B2P2K1/8 w - - 0 1", 1015133, 6);
    perft!("8/8/1k6/2b5/2pP4/8/5K2/8 b - d3 0 1", 1440467, 6);
    perft!("5k2/8/8/8/8/8/8/4K2R w K - 0 1", 661072, 6);
    perft!("3k4/8/8/8/8/8/8/R3K3 w Q - 0 1", 803711, 6);
    perft!("2K2r2/4P3/8/8/8/8/8/3k4 w - - 0 1", 3821001, 6);
    perft!("4k3/1P6/8/8/8/8/K7/8 w - - 0 1", 217342, 6);
    perft!("8/P1k5/K7/8/8/8/8/8 w - - 0 1", 92683, 6);
    perft!("K1k5/8/P7/8/8/8/8/8 w - - 0 1", 2217, 6);
    perft!(
        "r3k2r/Pppp1ppp/1b3nbN/nP6/BBP1P3/q4N2/Pp1P2PP/R2Q1RK1 w kq - 0 1",
        706045033,
        6
    );
    perft!(
        "r2q1rk1/pP1p2pp/Q4n2/bbp1p3/Np6/1B3NBn/pPPP1PPP/R3K2R b KQ - 0 1",
        706045033,
        6
    );
    perft!("8/2p5/3p4/KP5r/1R3p1k/8/4P1P1/8 w - - 0 1", 11030083, 6);
    perft!(
        "r3k2r/p1ppqpb1/bn2pnp1/3PN3/1p2P3/2N2Q1p/PPPBBPPP/R3K2R w KQkq - 0 1",
        8031647685,
        6
    );
    perft!(
        "rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1",
        119060324,
        6
    );
}

#[test]
fn depth_7() {
    perft!("8/k1P5/8/1K6/8/8/8/8 w - - 0 1", 567584, 7);
    perft!("8/2p5/3p4/KP5r/1R3p1k/8/4P1P1/8 w - - 0 1", 178633661, 7);
    perft!(
        "rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1",
        3195901860,
        7
    );
}

#[test]
fn depth_8() {
    perft!("8/2p5/3p4/KP5r/1R3p1k/8/4P1P1/8 w - - 0 1", 3009794393, 8);
    perft!(
        "rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1",
        84998978956,
        8
    );
}
