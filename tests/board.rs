use avocado::game::{
    board::{Board, UndoInfo},
    move_gen::MoveGen,
    move_list::MoveList,
};

macro_rules! undo_test {
    ($fen:tt, $gen:tt) => {
        let board = Board::new($fen).unwrap();
        let mut move_list = MoveList::default();

        $gen.gen_moves(&mut move_list, &board);

        let mut info = UndoInfo::default();

        for mv in &move_list {
            let mut test_board = board.clone();

            test_board.make_move(mv, &mut info);
            test_board.undo_move(mv, &info);

            if test_board != board {
                panic!(
                    "board was modified by move: {}\nunmodified: {:?}\nmodified: {:?}",
                    mv, board, test_board
                );
            }
        }
    };
}

#[test]
fn undo() {
    let gen = MoveGen::default();

    undo_test!(
        "r3k2r/Pppp1ppp/1b3nbN/nP6/BBP1P3/q4N2/Pp1P2PP/R2Q1R1K b kq - 1 1",
        gen
    );
    undo_test!("r6r/1b2k1bq/8/8/7B/8/8/R3K2R b QK - 3 2", gen);
    undo_test!("8/8/8/2k5/2pP4/8/B7/4K3 b - d3 5 3", gen);
    undo_test!(
        "r1bqkbnr/pppppppp/n7/8/8/P7/1PPPPPPP/RNBQKBNR w QqKk - 2 2",
        gen
    );
    undo_test!(
        "rnbq1k1r/pp1Pbppp/2p5/8/2B5/8/PPP1NnPP/RNBQK2R w KQ - 1 8",
        gen
    );
    undo_test!(
        "r3k2r/p1pp1pb1/bn2Qnp1/2qPN3/1p2P3/2N5/PPPBBPPP/R3K2R b QqKk - 3 2",
        gen
    );
    undo_test!(
        "rnb2k1r/pp1Pbppp/2p5/q7/2B5/8/PPPQNnPP/RNB1K2R w QK - 3 9",
        gen
    );
    undo_test!("2r5/3pk3/8/2P5/8/2K5/8/8 w - - 5 4", gen);
    undo_test!(
        "2kr3r/p1ppqpb1/bn2Qnp1/3PN3/1p2P3/2N5/PPPBBPPP/R3K2R b QK - 3 2",
        gen
    );
    undo_test!("4k3/8/8/5R2/8/8/8/4K3 b - - 0 1", gen);
    undo_test!("8/4k3/8/8/4R3/8/8/4K3 b - - 0 1", gen);
    undo_test!("4k3/6N1/5b2/4R3/8/8/8/4K3 b - - 0 1", gen);
    undo_test!("4k3/8/6n1/4R3/8/8/8/4K3 b - - 0 1", gen);
    undo_test!("8/8/8/2k5/3Pp3/8/8/4K3 b - d3 0 1", gen);
    undo_test!("8/8/8/1k6/3Pp3/8/8/4KQ2 b - d3 0 1", gen);
    undo_test!("4k3/8/4r3/8/8/4Q3/8/2K5 b - - 0 1", gen);
    undo_test!("8/8/8/8/k2Pp2Q/8/8/2K5 b - d3 0 1", gen);
    undo_test!(
        "rnbq1k1r/pp1Pbppp/2p5/8/2B5/8/PPP1NnPP/RNBQK2R w KQ - 1 8",
        gen
    );
    undo_test!(
        "r4rk1/1pp1qppp/p1np1n2/2b1p1B1/2B1P1b1/P1NP1N2/1PP1QPPP/R4RK1 w - - 0 10",
        gen
    );
    undo_test!(
        "r3k2r/Pppp1ppp/1b3nbN/nP6/BBP1P3/q4N2/Pp1P2PP/R2Q1RK1 w kq - 0 1",
        gen
    );
    undo_test!(
        "r2q1rk1/pP1p2pp/Q4n2/bbp1p3/Np6/1B3NBn/pPPP1PPP/R3K2R b KQ - 0 1",
        gen
    );
    undo_test!("8/2p5/3p4/KP5r/1R3p1k/8/4P1P1/8 w - - 0 1", gen);
    undo_test!(
        "r3k2r/p1ppqpb1/bn2pnp1/3PN3/1p2P3/2N2Q1p/PPPBBPPP/R3K2R w KQkq - 0 1",
        gen
    );
    undo_test!(
        "rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1",
        gen
    );
}
