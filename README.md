# `avocado`: A bitboard chess engine.

Usable from any UCI compatible chess gui.

## Features
 - All rules & moves implemented with a fast move generator
 - Magic bitboards for move generation
 - UCI
 - Iterative deepening, move ordering, negamax, null move pruning, etc. for searching
 - Currently a fairly basic evaluation system with piece-square tables
   for each game phase.

 ## TODO
 - Opening database
 - Better evaluation & search functions
 - Multithreaded search
