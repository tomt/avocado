//! An AST for UCI commands.

use crate::uci::{
    error::{UciError, UciResult},
    token::{Move, Tag, Token},
};

/// A parsed UCI command from the GUI.
#[derive(Debug)]
pub enum UciRequest<'a> {
    /// uci
    Uci,
    /// d (print the board)
    D,
    /// debug [ on | off ]
    Debug(bool),
    /// isready
    IsReady,
    /// setoption name <id> [value <x>]
    SetOption(&'a str, Option<Token<'a>>),
    /// register [ later | [ ( name <x> ) | ( code <y> ) ]+ ]
    Register(Register<'a>),
    /// ucinewgame
    UciNewGame,
    /// position [ ( fen <fen..> ) | startpos ] [moves <move..>]
    Position(Position<'a>, Moves),
    /// go
    Go(Go),
    /// stop
    Stop,
    /// ponderhit (user played expected move).
    PonderHit,
    /// quit
    Quit,
}

/// register
#[derive(Debug)]
pub enum Register<'a> {
    /// register later
    Later,
    /// register [name <name..>] [code <code>]
    Now {
        /// Name to register.
        name: Option<Token<'a>>,
        /// Code to register.
        code: Option<Token<'a>>,
    },
}

/// ( fen <fen..> ) | startpos
#[derive(Debug)]
pub enum Position<'a> {
    /// position startpos
    StartPos,
    /// position fen <fen..>
    Fen(Vec<Token<'a>>),
}

/// position <position> moves <moves..>
#[derive(Debug, Default)]
pub struct Moves(pub Vec<Move>);

/// The "go" command.
#[derive(Debug)]
pub enum Go {
    /// Run perft divide.
    Perft {
        /// Recursion depth.
        depth: u64,
    },
    /// Search for a move.
    Go {
        /// restrict search to these moves.
        searchmoves: Option<Moves>,
        /// ms left on white clock.
        wtime: Option<u64>,
        /// ms left on black clock.
        btime: Option<u64>,
        /// white time inc in ms.
        winc: Option<u64>,
        /// black time inc in ms.
        binc: Option<u64>,
        /// moves until next time control.
        movestogo: Option<u64>,
        /// only search to this depth.
        depth: Option<u64>,
        /// maximum nodes to search.
        nodes: Option<u64>,
        /// search for mate in this many moves.
        mate: Option<u64>,
        /// search for exactly this number of ms.
        movetime: Option<u64>,
        /// search until "stop" received.
        infinite: bool,
        /// start searching in ponder mode.
        ponder: bool,
    },
}

impl<'a> UciRequest<'a> {
    /// Parses tokens from stdin.
    pub fn parse(mut tokens: impl Iterator<Item = Token<'a>>) -> UciResult<Self> {
        // read tokens until a valid command is encountered.
        while let Some(Token(cmd, _)) = tokens.next() {
            return match cmd {
                "uci" => Ok(UciRequest::Uci),
                "d" => Ok(UciRequest::D),
                "debug" => Self::parse_debug(tokens),
                "isready" => Ok(UciRequest::IsReady),
                "setoption" => Self::parse_setoption(tokens),
                "register" => Self::parse_register(tokens),
                "ucinewgame" => Ok(UciRequest::UciNewGame),
                "position" => Self::parse_position(tokens),
                "go" => Self::parse_go(tokens),
                "stop" => Ok(UciRequest::Stop),
                "ponderhit" => Ok(UciRequest::PonderHit),
                "quit" => Ok(UciRequest::Quit),
                // read next token.
                _ => continue,
            };
        }

        Err(UciError::MissingCmd)
    }

    /// Gets a Str token.
    fn get_str(tokens: &mut impl Iterator<Item = Token<'a>>) -> UciResult<&'a str> {
        tokens
            .next()
            .map(|Token(token, tag)| match tag {
                Tag::Empty => "",
                _ => token,
            })
            .ok_or(UciError::MissingToken)
    }
    /// Gets a Int token.
    fn get_int(tokens: &mut impl Iterator<Item = Token<'a>>) -> UciResult<u64> {
        match tokens.next() {
            Some(Token(_, Tag::Int(i))) => Ok(i),
            _ => Err(UciError::MissingToken),
        }
    }
    /// Gets a Bool token.
    fn get_bool(tokens: &mut impl Iterator<Item = Token<'a>>) -> UciResult<bool> {
        match tokens.next() {
            Some(Token(_, Tag::Bool(b))) => Ok(b),
            _ => Err(UciError::MissingToken),
        }
    }

    /// Expects a specific token.
    fn expect_str(t: &str, tokens: &mut impl Iterator<Item = Token<'a>>) -> UciResult<()> {
        match Self::exists_str(t, tokens) {
            true => Ok(()),
            _ => Err(UciError::UnexpectedToken),
        }
    }
    /// Checks whether the token exists (as the next value).
    fn exists_str(t: &str, tokens: &mut impl Iterator<Item = Token<'a>>) -> bool {
        tokens.next() == Some(Token(t, Tag::Str))
    }

    /// Reads all moves from the token stream.
    fn read_moves(tokens: &mut impl Iterator<Item = Token<'a>>) -> (Moves, Option<Token<'a>>) {
        let mut token;
        let mut moves = vec![];

        // read all moves.
        loop {
            token = tokens.next();
            match token {
                Some(Token(_, Tag::Move(mv))) => moves.push(mv),
                _ => break,
            }
        }

        (Moves(moves), token)
    }

    /// Gets the next token.
    fn next(tokens: &mut impl Iterator<Item = Token<'a>>) -> UciResult<Token<'a>> {
        tokens.next().ok_or(UciError::MissingToken)
    }

    /// Parses the debug command.
    fn parse_debug(mut tokens: impl Iterator<Item = Token<'a>>) -> UciResult<Self> {
        Ok(UciRequest::Debug(Self::get_bool(&mut tokens)?))
    }
    /// Parses the setoption command.
    fn parse_setoption(mut tokens: impl Iterator<Item = Token<'a>>) -> UciResult<Self> {
        Self::expect_str("name", &mut tokens)?;
        let name = Self::get_str(&mut tokens)?;
        let value = match Self::exists_str("value", &mut tokens) {
            true => Some(Self::next(&mut tokens)?),
            false => None,
        };

        Ok(UciRequest::SetOption(name, value))
    }
    /// Parses the register command.
    fn parse_register(mut tokens: impl Iterator<Item = Token<'a>>) -> UciResult<Self> {
        let mut token = Self::get_str(&mut tokens)?;

        match token {
            "later" => Ok(UciRequest::Register(Register::Later)),
            _ => {
                let mut name = None;
                let mut code = None;

                // loop until end.
                loop {
                    match token {
                        "name" => name = Some(Self::next(&mut tokens)?),
                        "code" => code = Some(Self::next(&mut tokens)?),
                        _ => return Err(UciError::UnexpectedToken),
                    }

                    match Self::get_str(&mut tokens) {
                        Ok(t) => token = t,
                        Err(_) => break,
                    }
                }

                Ok(UciRequest::Register(Register::Now { name, code }))
            }
        }
    }
    /// Parses the position command.
    fn parse_position(mut tokens: impl Iterator<Item = Token<'a>>) -> UciResult<Self> {
        let position = match Self::get_str(&mut tokens)? {
            "startpos" => Position::StartPos,
            "fen" => Position::Fen(
                (0..6)
                    .map(|_| Self::next(&mut tokens))
                    .collect::<UciResult<_>>()?,
            ),
            _ => return Err(UciError::MissingToken),
        };

        let moves = Self::exists_str("moves", &mut tokens)
            .then(|| Self::read_moves(&mut tokens))
            .map(|(moves, _)| moves)
            .unwrap_or_default();

        Ok(UciRequest::Position(position, moves))
    }
    /// Parses the go command.
    fn parse_go(mut tokens: impl Iterator<Item = Token<'a>>) -> UciResult<Self> {
        match Self::get_str(&mut tokens) {
            Ok("perft") => Ok(UciRequest::Go(Go::Perft {
                depth: Self::get_int(&mut tokens)?,
            })),
            token => {
                let mut searchmoves = None;
                let mut wtime = None;
                let mut btime = None;
                let mut winc = None;
                let mut binc = None;
                let mut movestogo = None;
                let mut depth = None;
                let mut nodes = None;
                let mut mate = None;
                let mut movetime = None;
                let mut infinite = false;
                let mut ponder = false;

                if let Ok(mut token) = token {
                    loop {
                        match token {
                            "searchmoves" => {
                                let (moves, next_token) = Self::read_moves(&mut tokens);
                                searchmoves = Some(moves);

                                match next_token {
                                    Some(Token(t, _)) => token = t,
                                    _ => break,
                                }

                                // must skip advancing to next token.
                                continue;
                            }
                            "wtime" => wtime = Some(Self::get_int(&mut tokens)?),
                            "btime" => btime = Some(Self::get_int(&mut tokens)?),
                            "winc" => winc = Some(Self::get_int(&mut tokens)?),
                            "binc" => binc = Some(Self::get_int(&mut tokens)?),
                            "movestogo" => movestogo = Some(Self::get_int(&mut tokens)?),
                            "depth" => depth = Some(Self::get_int(&mut tokens)?),
                            "nodes" => nodes = Some(Self::get_int(&mut tokens)?),
                            "mate" => mate = Some(Self::get_int(&mut tokens)?),
                            "movetime" => movetime = Some(Self::get_int(&mut tokens)?),
                            "infinite" => infinite = true,
                            "ponder" => ponder = true,
                            _ => return Err(UciError::UnexpectedToken),
                        }

                        match Self::get_str(&mut tokens) {
                            Ok(t) => token = t,
                            Err(_) => break,
                        }
                    }
                }

                Ok(UciRequest::Go(Go::Go {
                    searchmoves,
                    wtime,
                    btime,
                    winc,
                    binc,
                    movestogo,
                    depth,
                    nodes,
                    mate,
                    movetime,
                    infinite,
                    ponder,
                }))
            }
        }
    }
}
