//! Messages sent to the GUI.

use crate::{engine::pv::Pv, game::moves::Move};
use std::fmt;

/// Messages sent to stdout.
///
/// From: https://backscattering.de/chess/uci/
#[derive(Debug)]
pub enum UciResponse {
    /// id [ name | author ]
    Id(Id),
    /// uciok
    UciOk,
    /// readyok
    ReadyOk,
    /// best move and option (ponder move)
    BestMove(Move, Option<Move>),
    /// copyprotection
    CopyProtection(Status),
    /// registration
    Registration(Status),
    /// info
    Info(Box<Info>),
    /// option (name, type).
    Option(&'static str, Type),
    /// printing out the board and hash.
    D(String),

    /// output from perft.
    Perft(Perft),
    /// custom debug message.
    Debug(DebugLevel, String),
}

/// Output from perft.
#[derive(Debug)]
pub enum Perft {
    /// A line from perft divide, contains the move and corresponding
    /// node count.
    Divide(Move, u64),
    /// The overall node count from perft.
    NodesSearched(u64),
}

/// The UCI info response.
#[derive(Debug, Default)]
pub struct Info {
    /// search depth
    pub depth: Option<usize>,
    /// selectuve search depth
    pub seldepth: Option<u8>,
    /// time searched in ms
    pub time: Option<u16>,
    /// nodes searched
    pub nodes: Option<u32>,
    /// for multipv mode
    pub multipv: Option<u8>,
    /// the score of the position
    pub score: Option<(Score, Option<Bound>)>,
    /// current move being searched
    pub currmove: Option<Move>,
    /// current line the engine is calculating and cpu number.
    ///
    /// only send if UCI_ShowCurrLine enabled.
    pub currline: Option<(Option<u8>, Pv)>,
    /// currently searching move number <x>, first move is 1.
    pub currmovenumber: Option<u32>,
    /// hash is <x> permill full
    pub hashfull: Option<u32>,
    /// nodes per second searched
    pub nps: Option<u32>,
    /// endgame table hits
    pub tbhits: Option<u16>,
    /// shredder endgame db hits
    pub sbhits: Option<u16>,
    /// cpu usage of engine permill
    pub cpuload: Option<u16>,
    /// best line found
    pub pv: Option<Pv>,
    /// string displayed to the gui
    pub string: Option<String>,
    /// ({move refuted}, {by these moves})
    ///
    /// only send if UCI_ShowRefutations enabled
    pub refutation: Option<(Move, Vec<Move>)>,
}

/// Debug level.
#[derive(Debug)]
pub enum DebugLevel {
    /// Trace, info and error messages.
    Trace,
    /// Info and error messages.
    Info,
    /// Only error messages.
    Error,
}

/// Option type.
#[derive(Debug)]
pub enum Type {
    /// Checkbox.
    Check {
        /// default value.
        default: Option<bool>,
    },
    /// Spin wheel, integer in a range.
    Spin {
        /// default value.
        default: Option<i32>,
        /// min value.
        min: Option<i32>,
        /// max value.
        max: Option<i32>,
    },
    /// Combo box with predefined options.
    Combo {
        /// A default option.
        default: Option<&'static str>,
        /// The predefined options.
        vars: Vec<&'static str>,
    },
    /// Button, pressed to send a command.
    Button,
    /// A text field with a string value.
    String {
        /// default value.
        default: Option<&'static str>,
    },
}

/// Variants for id command.
#[derive(Debug)]
pub enum Id {
    /// id name <..>
    Name(&'static str),
    /// id author <..>
    Author(&'static str),
}

/// Status for copyprotection and registration.
#[derive(Debug)]
pub enum Status {
    /// Checking.
    Checking,
    /// Ok: succeeded.
    Ok,
    /// Error occured.
    Error,
}

/// Score type.
#[derive(Debug)]
pub enum Score {
    /// Centipawns.
    Cp(i32),
    /// Ply until mate.
    Mate(u8),
}

/// Whether score is a lower/upper bound.
#[derive(Debug)]
pub enum Bound {
    /// Lower bound.
    Lower,
    /// Upper bound.
    Upper,
}

impl fmt::Display for UciResponse {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            UciResponse::Perft(Perft::Divide(mv, nodes)) => write!(f, "{mv}: {nodes}"),
            UciResponse::Perft(Perft::NodesSearched(nodes)) => {
                write!(f, "\nnodes searched: {nodes}")
            }
            UciResponse::D(d) => write!(f, "{d}"),
            UciResponse::Debug(DebugLevel::Trace, msg) => write!(f, "[trace] {msg}"),
            UciResponse::Debug(DebugLevel::Info, msg) => write!(f, "[ info] {msg}"),
            UciResponse::Debug(DebugLevel::Error, msg) => write!(f, "[error] {msg}"),
            UciResponse::Id(Id::Name(name)) => write!(f, "id name {name}"),
            UciResponse::Id(Id::Author(author)) => write!(f, "id author {author}"),
            UciResponse::UciOk => write!(f, "uciok"),
            UciResponse::ReadyOk => write!(f, "readyok"),
            UciResponse::BestMove(best, None) => write!(f, "bestmove {best}"),
            UciResponse::BestMove(best, Some(ponder)) => {
                write!(f, "bestmove {best} ponder {ponder}")
            }
            UciResponse::CopyProtection(Status::Checking) => write!(f, "copyprotection checking"),
            UciResponse::CopyProtection(Status::Ok) => write!(f, "copyprotection ok"),
            UciResponse::CopyProtection(Status::Error) => write!(f, "copyprotection error"),
            UciResponse::Registration(Status::Checking) => write!(f, "registration checking"),
            UciResponse::Registration(Status::Ok) => write!(f, "registration ok"),
            UciResponse::Registration(Status::Error) => write!(f, "registration error"),
            UciResponse::Info(info) => {
                let Info {
                    depth,
                    seldepth,
                    time,
                    nodes,
                    pv,
                    multipv,
                    score,
                    currmove,
                    currmovenumber,
                    hashfull,
                    nps,
                    tbhits,
                    sbhits,
                    cpuload,
                    currline,
                    string,
                    refutation,
                } = info.as_ref();

                write!(f, "info")?;

                if let Some(depth) = depth {
                    write!(f, " depth {depth}")?;
                }
                if let Some(seldepth) = seldepth {
                    write!(f, " seldepth {seldepth}")?;
                }
                if let Some(time) = time {
                    write!(f, " time {time}")?;
                }
                if let Some(nodes) = nodes {
                    write!(f, " nodes {nodes}")?;
                }
                if let Some(multipv) = multipv {
                    write!(f, " multipv {multipv}")?;
                }
                if let Some((score, bound)) = score {
                    match score {
                        Score::Cp(cp) => write!(f, " score cp {cp}")?,
                        Score::Mate(mate) => write!(f, " score mate {mate}")?,
                    }

                    if let Some(bound) = bound {
                        match bound {
                            Bound::Lower => write!(f, " upperbound")?,
                            Bound::Upper => write!(f, " lowerbound")?,
                        }
                    }
                }
                if let Some(currmove) = currmove {
                    write!(f, " currmove {currmove}")?;
                }
                if let Some(currmovenumber) = currmovenumber {
                    write!(f, " currmovenumber {currmovenumber}")?;
                }
                if let Some(hashfull) = hashfull {
                    write!(f, " hashfull {hashfull}")?;
                }
                if let Some(nps) = nps {
                    write!(f, " nps {nps}")?;
                }
                if let Some(tbhits) = tbhits {
                    write!(f, " tbhits {tbhits}")?;
                }
                if let Some(sbhits) = sbhits {
                    write!(f, " sbhits {sbhits}")?;
                }
                if let Some(cpuload) = cpuload {
                    write!(f, " cpuload {cpuload}")?;
                }
                if let Some((cpunr, currline)) = currline {
                    write!(f, " currline")?;

                    if let Some(cpunr) = cpunr {
                        write!(f, " {cpunr}")?;
                    }

                    write!(f, "{currline}")?;
                }
                if let Some(string) = string {
                    write!(f, " string {string}")?;
                }
                if let Some((mv, refutation)) = refutation {
                    write!(f, " refutation {mv}")?;

                    for mv in refutation {
                        write!(f, " {mv}")?;
                    }
                }
                if let Some(pv) = pv {
                    write!(f, " pv{pv}")?;
                }

                Ok(())
            }
            UciResponse::Option(name, Type::Button) => {
                write!(f, "option name {name} type button")
            }
            UciResponse::Option(name, Type::Check { default }) => {
                write!(f, "option name {name} type check")?;

                if let Some(default) = default {
                    write!(f, " default {default}")?;
                }

                Ok(())
            }
            UciResponse::Option(name, Type::Combo { default, vars }) => {
                write!(f, "option name {name} type combo")?;

                if let Some(default) = default {
                    write!(f, " default {default}")?;
                }

                for var in vars {
                    write!(f, " var {var}")?;
                }

                Ok(())
            }
            UciResponse::Option(name, Type::Spin { default, min, max }) => {
                write!(f, "option name {name} type spin")?;

                if let Some(default) = default {
                    write!(f, " default {default}")?;
                }
                if let Some(min) = min {
                    write!(f, " min {min}")?;
                }
                if let Some(max) = max {
                    write!(f, " max {max}")?;
                }

                Ok(())
            }
            UciResponse::Option(name, Type::String { default }) => {
                write!(f, "option name {name} type string")?;

                if let Some(default) = default {
                    write!(f, " default {default}")?;
                }

                Ok(())
            }
        }
    }
}
