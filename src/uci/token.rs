//! Tokenizing UCI commands.

use crate::{
    game::{moves::PromotionPiece, square::Square},
    uci::error::{UciError, UciResult},
};
use std::fmt;

/// Token definition for parsing UCI commands.
///
///  e.g:
/// ```md,ignore
///     go perft 1
///     [
///         Str("go")
///         Str("perft"),
///         Int(1),
///     ]
///
///     position startpos moves e2e4
///     [
///         Str("position"),
///         Str("startpos"),
///         Str("moves"),
///         Move(E2, E4, None),
///     ]
/// ```
#[derive(Debug, PartialEq, Eq)]
pub struct Token<'a>(pub &'a str, pub Tag);

/// A tag that is attached to a token.
#[derive(Debug, PartialEq, Eq)]
pub enum Tag {
    /// Boolean.
    Bool(bool),
    /// Integer.
    Int(u64),
    /// Move.
    Move(Move),
    /// Empty string.
    Empty,
    /// String.
    Str,
}

impl<'a> fmt::Display for Token<'a> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let &Token(token, _) = self;
        write!(f, "{token}")
    }
}

/// The move token.
#[derive(Debug, PartialEq, Eq)]
pub struct Move(pub Square, pub Square, pub Option<PromotionPiece>);

/// Tokenizes a UCI command.
pub fn tokenize(cmd: &str) -> impl Iterator<Item = Token<'_>> {
    cmd.split_whitespace().map(|token| {
        Token(
            token,
            parse_int(token)
                .or_else(|_| parse_bool(token))
                .or_else(|_| parse_move(token))
                .or_else(|_| parse_empty(token))
                .unwrap_or(Tag::Str),
        )
    })
}

/// Attempts to parse string as empty.
fn parse_empty(s: &str) -> UciResult<Tag> {
    match s {
        "<empty>" => Ok(Tag::Empty),
        _ => Err(UciError::UnexpectedToken),
    }
}

/// Attempts to parse string as a boolean.
fn parse_bool(s: &str) -> UciResult<Tag> {
    match s {
        "true" | "on" | "y" | "t" => Ok(Tag::Bool(true)),
        "false" | "off" | "n" | "f" => Ok(Tag::Bool(false)),
        _ => Err(UciError::InvalidBool),
    }
}

/// Attempts to parse string as an integer.
fn parse_int(s: &str) -> UciResult<Tag> {
    s.parse::<u64>()
        .map(Tag::Int)
        .map_err(|_| UciError::InvalidInt)
}

/// Attempts to parse string as a move.
fn parse_move(s: &str) -> UciResult<Tag> {
    let bytes = s.as_bytes();

    match (4..=5).contains(&bytes.len()) {
        false => Err(UciError::InvalidMove),
        true => Ok(Tag::Move(Move(
            Square::try_from((bytes[1], bytes[0])).map_err(UciError::SquareParse)?,
            Square::try_from((bytes[3], bytes[2])).map_err(UciError::SquareParse)?,
            match bytes.get(4) {
                None => None,
                Some(&b) => {
                    Some(PromotionPiece::try_from(b).map_err(|_| UciError::PromotionParse)?)
                }
            },
        ))),
    }
}
