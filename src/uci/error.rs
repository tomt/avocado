//! Error types for UCI.

use crate::error::Error;
use std::{fmt, sync::mpsc::SendError};

use super::response::UciResponse;

/// The result type for the UCI module.
pub type UciResult<T> = std::result::Result<T, UciError>;

/// An error from parsing a UCI command.
#[derive(Debug)]
pub enum UciError {
    /// Move was not legal or was malformed.
    InvalidMove,
    /// Malformed bool value.
    InvalidBool,
    /// Failed to parse integer.
    InvalidInt,
    /// Unexpected.
    UnexpectedToken,
    /// Expected a token to follow.
    MissingToken,
    /// No matched command was supplied.
    MissingCmd,
    /// Failed to parse promotion.
    PromotionParse,
    /// Failed to parse FEN string.
    FenParse(Error),
    /// Failed to parse move.
    SquareParse(Error),
    /// Failed to send message to UCI response thread.
    SendError,
}

impl fmt::Display for UciError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            UciError::InvalidBool => write!(f, "Invalid bool"),
            UciError::InvalidMove => write!(f, "Invalid move"),
            UciError::InvalidInt => write!(f, "Expected unsigned integer"),
            UciError::UnexpectedToken => write!(f, "Unexpected token"),
            UciError::MissingToken => write!(f, "Missing token"),
            UciError::MissingCmd => write!(f, "Missing command"),
            UciError::PromotionParse => write!(f, "Failed to parse promotion"),
            UciError::FenParse(e) => write!(f, "FEN: {e}"),
            UciError::SquareParse(e) => write!(f, "Square: {e}"),
            UciError::SendError => write!(f, "Failed to send message to UCI response thread"),
        }
    }
}
impl std::error::Error for UciError {}
impl From<SendError<UciResponse>> for UciError {
    fn from(_: SendError<UciResponse>) -> Self {
        Self::SendError
    }
}
