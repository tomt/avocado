//! Module for UCI communication.

use crate::{
    engine::{perft, search::Search},
    game::{
        board::{Board, UndoInfo},
        move_gen::MoveGen,
        move_list::MoveList,
    },
    uci::{
        error::{UciError, UciResult},
        request::{Go, Moves, Position, UciRequest},
        response::{DebugLevel, Id, UciResponse},
        token::Move,
    },
};
use std::{
    io::BufRead,
    sync::{
        atomic::{AtomicBool, Ordering},
        mpsc, Arc,
    },
    thread,
};

pub mod error;
pub mod request;
pub mod response;
pub mod token;

/// Manages UCI communication and the main engine loop.
#[derive(Debug)]
pub struct Uci {
    /// Whether to print debug messages.
    debug: Arc<AtomicBool>,

    /// Current state of the board.
    board: Board,
    /// Move generator, shared between threads.
    gen: Arc<MoveGen>,

    /// Optional thread for computing perft.
    perft: Option<thread::JoinHandle<()>>,
    /// Optional iterative deepening thread.
    search: Option<thread::JoinHandle<()>>,

    /// Send half of channel for UCI messages sent to stdout.
    uci_tx: mpsc::Sender<UciResponse>,

    /// Whether to stop the search.
    stop: Arc<AtomicBool>,
}

impl Default for Uci {
    fn default() -> Self {
        // mpsc queue for uci responses.
        let (uci_tx, uci_rx) = mpsc::channel();
        let debug = Arc::new(AtomicBool::new(false));

        // uci response thread.
        thread::spawn({
            let debug = debug.clone();

            move || {
                // listen for any UCI response messages, and print them to stdout.
                for uci_response in uci_rx {
                    if let r @ UciResponse::Debug(_, _) = uci_response {
                        if debug.load(Ordering::Relaxed) {
                            println!("{r}");
                        }
                    } else {
                        println!("{uci_response}");
                    }
                }
            }
        });

        Self {
            debug,

            board: Board::default(),
            gen: Arc::new(MoveGen::default()),

            perft: None,
            search: None,

            uci_tx,

            stop: Arc::new(AtomicBool::new(false)),
        }
    }
}
impl Uci {
    /// The main thread for UCI. Reads stdin line-by-line and sends messages
    /// to the other threads that it manages.
    pub fn run(&mut self) {
        let stdin = std::io::stdin().lock();

        for line in stdin.lines().flatten() {
            match self.run_line(&line) {
                Ok(true) => break,
                Err(e) => self
                    .uci_tx
                    .send(UciResponse::Debug(DebugLevel::Error, format!("Err({e:?})")))
                    .unwrap(),
                _ => (),
            }
        }
    }

    /// Stops perft and search threads.
    fn stop(&mut self) {
        // signal that search should stop.
        self.stop.store(true, Ordering::Relaxed);

        // join thread handles.
        if let Some(perft) = self.perft.take() {
            perft.join().unwrap();
        }
        if let Some(search) = self.search.take() {
            search.join().unwrap();
        }

        // reset stop.
        self.stop.store(false, Ordering::Relaxed);
    }

    /// Handles a line of input from UCI. Return value indicates whether
    /// to exit the loop.
    pub fn run_line(&mut self, line: &str) -> UciResult<bool> {
        let cmd = UciRequest::parse(token::tokenize(line))?;
        self.uci_tx
            .send(UciResponse::Debug(DebugLevel::Trace, format!("{cmd:?}")))?;

        // remove any threads that have finished.
        self.search = match self.search.take() {
            Some(search) if !search.is_finished() => Some(search),
            _ => None,
        };
        self.perft = match self.perft.take() {
            Some(perft) if !perft.is_finished() => Some(perft),
            _ => None,
        };

        match cmd {
            UciRequest::Uci => {
                self.uci_tx.send(UciResponse::Id(Id::Name("Avocado")))?;
                self.uci_tx
                    .send(UciResponse::Id(Id::Author("@upsidedown")))?;
                self.uci_tx.send(UciResponse::UciOk)?;
            }
            UciRequest::D => self.uci_tx.send(UciResponse::D(format!(
                "{}\nfen: {}\nzobrist: {}",
                self.board,
                self.board.fen(),
                u64::from(self.board.zobrist())
            )))?,
            UciRequest::Debug(debug) => self.debug.store(debug, Ordering::Relaxed),
            UciRequest::IsReady => {
                if self.search.is_none() && self.perft.is_none() {
                    self.uci_tx.send(UciResponse::ReadyOk)?;
                }
            }
            UciRequest::SetOption(_, _) => todo!(),
            UciRequest::Register(_) => todo!(),
            UciRequest::UciNewGame => {
                self.stop();
                self.board = Board::default()
            }
            UciRequest::Position(position, Moves(moves)) => {
                self.stop();
                let mut new_board = match position {
                    Position::StartPos => Board::default(),
                    Position::Fen(tokens) => {
                        let fen = tokens.into_iter().map(|t| t.to_string()).fold(
                            String::new(),
                            |mut acc, x| {
                                acc += &x;
                                acc.push(' ');
                                acc
                            },
                        );
                        Board::new(&fen).map_err(UciError::FenParse)?
                    }
                };

                let mut info = UndoInfo::default();
                let mut move_list = MoveList::default();

                'outer: for Move(start, end, pro) in moves {
                    self.gen.gen_moves(&mut move_list, &new_board);

                    for mv in &move_list {
                        if mv.start() == start
                            && mv.end() == end
                            && (pro.is_none() || pro == Some(mv.promotion_piece()))
                        {
                            new_board.make_move(mv, &mut info);
                            continue 'outer;
                        }
                    }

                    // move not matched.
                    return Err(UciError::InvalidMove);
                }

                self.board = new_board;
            }
            UciRequest::Go(Go::Perft { depth }) => {
                self.stop();

                let tx = self.uci_tx.clone();
                let gen = self.gen.clone();
                let stop = self.stop.clone();
                let mut board = self.board.clone();

                // set the join handle.
                self.perft = Some(thread::spawn(move || {
                    perft::perft_divide(
                        (depth as usize).clamp(1, 10),
                        &mut board,
                        &gen,
                        tx,
                        Some(&stop),
                    )
                    .unwrap();
                }))
            }
            UciRequest::Go(Go::Go {
                searchmoves: _,
                wtime: _,
                btime: _,
                winc: _,
                binc: _,
                movestogo: _,
                depth,
                nodes: _,
                mate: _,
                movetime,
                infinite: _,
                ponder: _,
            }) => {
                self.stop();

                let depth = depth.map(|d| d as usize).unwrap_or(8);
                let time = movetime.map(|t| t as usize).unwrap_or(5_000);
                let uci_tx = self.uci_tx.clone();
                let gen = self.gen.clone();
                let stop = self.stop.clone();
                let board = self.board.clone();

                self.search = Some(thread::spawn(move || {
                    let mut search =
                        Search::new(board, &gen, &stop, move |msg| uci_tx.send(msg).unwrap());
                    search.iterative_deepening(depth, time);
                }));
            }
            UciRequest::Stop => self.stop(),
            UciRequest::PonderHit => todo!(),
            UciRequest::Quit => {
                self.stop();
                return Ok(true);
            }
        }

        Ok(false)
    }
}
