//! Avocado: A zero-dependency UCI Chess engine.

#![warn(missing_docs)]
#![deny(unsafe_code)]

pub mod engine;
pub mod error;
pub mod game;
pub mod uci;
