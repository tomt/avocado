//! Rook and Bishop magics for O(1) rook,bishop and queen move generation.

use crate::game::{
    bitboard::BitBoard,
    color::Color,
    square::{Direction, Square},
};

/// A lookup to generate moves for each type of piece.
#[derive(Debug)]
pub struct Magics {
    /// Occupancy masks for rook magics
    rook_masks: [BitBoard; 64],
    /// Occupancy masks for bishop magics
    bishop_masks: [BitBoard; 64],
    /// Shifts for rook magics
    rook_magic_shifts: [u32; 64],
    /// Shifts for bishop magics
    bishop_magic_shifts: [u32; 64],
    /// Rook magic lookup.
    rook_moves: Vec<BitBoard>,
    /// Bishop magic lookup.
    bishop_moves: Vec<BitBoard>,

    /// Knight move lookup.
    knight_moves: [BitBoard; 64],
    /// King move lookup.
    king_moves: [BitBoard; 64],

    /// The positions that enemy pawns could attack a square from.
    pawn_attacks: [[BitBoard; 64]; 2],

    /// Squares between `start` and `end` on `slider_range[start][end]`,
    /// * not including `start`
    /// * including `end`
    rays: [[BitBoard; 64]; 64],
}

impl Magics {
    /// Initialises the lookup tables.
    pub fn new() -> Magics {
        let mut rook_masks = [BitBoard::EMPTY; 64];
        let mut bishop_masks = [BitBoard::EMPTY; 64];
        let mut rook_magic_shifts = [0; 64];
        let mut bishop_magic_shifts = [0; 64];
        let mut rook_moves = vec![BitBoard::EMPTY; 4096 * 64];
        let mut bishop_moves = vec![BitBoard::EMPTY; 4096 * 64];

        let mut knight_moves = [BitBoard::EMPTY; 64];
        let mut king_moves = [BitBoard::EMPTY; 64];

        let mut pawn_attacks = [[BitBoard::EMPTY; 64]; 2];
        let mut rays = [[BitBoard::EMPTY; 64]; 64];

        // masks, shifts and move tables
        for (i, sq) in Square::iter().enumerate() {
            knight_moves[i] = gen_knight_moves(sq);
            king_moves[i] = gen_king_moves(sq);

            rook_masks[i] = gen_slider_mask(sq, Direction::iter_rook());
            bishop_masks[i] = gen_slider_mask(sq, Direction::iter_bishop());
            rook_magic_shifts[i] = rook_masks[i].count_zeros();
            bishop_magic_shifts[i] = bishop_masks[i].count_zeros();

            // rook & bishop move tables
            for idx in 0..(1 << rook_masks[i].count_ones()) {
                let indexed_mask = idx_to_bitboard(idx, rook_masks[i]);
                let key = (ROOK_MAGICS[i]
                    .wrapping_mul(u64::from(indexed_mask))
                    .wrapping_shr(rook_magic_shifts[i])) as usize;
                rook_moves[i * 4096 + key] =
                    gen_slider_moves(sq, indexed_mask, Direction::iter_rook());
            }
            for idx in 0..(1 << bishop_masks[i].count_ones()) {
                let indexed_mask = idx_to_bitboard(idx, bishop_masks[i]);
                let key = (BISHOP_MAGICS[i]
                    .wrapping_mul(u64::from(indexed_mask))
                    .wrapping_shr(bishop_magic_shifts[i])) as usize;
                bishop_moves[i * 4096 + key] =
                    gen_slider_moves(sq, indexed_mask, Direction::iter_bishop());
            }

            // pawn attacks
            if sq.rank() > 0 {
                for sq in [Direction::NorthEast, Direction::NorthWest]
                    .into_iter()
                    .filter_map(|dir| sq.neighbour(dir))
                {
                    pawn_attacks[usize::from(Color::White)][i].set(sq);
                }
            }
            if sq.rank() < 7 {
                for sq in [Direction::SouthEast, Direction::SouthWest]
                    .into_iter()
                    .filter_map(|dir| sq.neighbour(dir))
                {
                    pawn_attacks[usize::from(Color::Black)][i].set(sq);
                }
            }
        }

        // rays
        for start in Square::iter() {
            for end in Square::iter() {
                rays[usize::from(start)][usize::from(end)] = gen_ray(start, end);
            }
        }

        Magics {
            rook_masks,
            bishop_masks,
            rook_magic_shifts,
            bishop_magic_shifts,
            rook_moves,
            bishop_moves,
            knight_moves,
            king_moves,
            pawn_attacks,
            rays,
        }
    }

    /// Gets the ray connecting `start` and `end`, not including `start`.
    pub fn ray(&self, start: Square, end: Square) -> BitBoard {
        self.rays[usize::from(start)][usize::from(end)]
    }
    /// Gets the possible squares that a pawn could attack `sq` from.
    pub fn pawn_attacks(&self, sq: Square, color: Color) -> BitBoard {
        self.pawn_attacks[usize::from(color)][usize::from(sq)]
    }
    /// Gets the knight moves from `sq`.
    pub fn knight(&self, sq: Square) -> BitBoard {
        self.knight_moves[usize::from(sq)]
    }
    /// Gets the king moves from `sq`.
    pub fn king(&self, sq: Square) -> BitBoard {
        self.king_moves[usize::from(sq)]
    }
    /// Gets the bishop moves from `sq`.
    pub fn bishop(&self, sq: Square, occupancy: BitBoard) -> BitBoard {
        let sq = usize::from(sq);
        let key = key(
            occupancy,
            BISHOP_MAGICS[sq],
            self.bishop_masks[sq],
            self.bishop_magic_shifts[sq],
        );

        self.bishop_moves[sq * 4096 + key]
    }
    /// Gets the rook moves from `sq`.
    pub fn rook(&self, sq: Square, occupancy: BitBoard) -> BitBoard {
        let sq = usize::from(sq);
        let key = key(
            occupancy,
            ROOK_MAGICS[sq],
            self.rook_masks[sq],
            self.rook_magic_shifts[sq],
        );

        self.rook_moves[sq * 4096 + key]
    }
    /// Gets the queen moves from `sq`.
    pub fn queen(&self, sq: Square, occupancy: BitBoard) -> BitBoard {
        self.bishop(sq, occupancy) | self.rook(sq, occupancy)
    }
}
impl Default for Magics {
    fn default() -> Self {
        Self::new()
    }
}

/// Calculates the lookup key from the occupancy, mask, magic and shift.
#[inline(always)]
pub fn key(occ: BitBoard, magic: BitBoard, mask: BitBoard, shr: u32) -> usize {
    let masked_occupancy = u64::from(occ & mask);
    ((magic.wrapping_mul(masked_occupancy)).wrapping_shr(shr)) as usize
}

/// Creates an occupancy mask for a slider piece from a start square
/// and iterator over directions.
pub fn gen_slider_mask(start: Square, iter: impl Iterator<Item = Direction>) -> BitBoard {
    let mut mask = BitBoard::EMPTY;

    for dir in iter {
        let mut ray = start.ray(dir).peekable();

        while let Some(sq) = ray.next() {
            if ray.peek().is_some() {
                mask.set(sq);
            }
        }
    }

    mask
}
/// Converts an index to a bitboard by setting some of the bits
/// on the provided mask.
pub fn idx_to_bitboard(idx: usize, mask: BitBoard) -> BitBoard {
    mask.into_iter()
        .enumerate()
        .filter(|(i, _)| idx & (1 << *i) != 0)
        .map(|(_, sq)| sq)
        .collect()
}
/// Generates a bitboard containing slider moves from `start` in each of the
/// directions of the iterator, respecting the board occupancy.
pub fn gen_slider_moves(
    start: Square,
    occ: BitBoard,
    iter: impl Iterator<Item = Direction>,
) -> BitBoard {
    iter.flat_map(|dir| {
        let mut collided = false;
        start
            .ray(dir)
            .take_while(move |&sq| !std::mem::replace(&mut collided, occ.contains(sq)))
    })
    .collect()
}

/// Generates a bitboard containing possible squares a king could
/// move to from `start`.
fn gen_king_moves(start: Square) -> BitBoard {
    Direction::iter()
        .filter_map(|dir| start.neighbour(dir))
        .collect()
}
/// Generates a bitboard containing possible squares a knight could
/// move to from `start`.
fn gen_knight_moves(start: Square) -> BitBoard {
    let rank = start.rank();
    let file = start.file();

    KNIGHT_VECTORS
        .into_iter()
        .map(|(dr, df)| (rank + dr, file + df))
        .filter_map(Square::new)
        .collect()
}
/// Generates the bitboard ray between two squares.
fn gen_ray(start: Square, end: Square) -> BitBoard {
    start.to(end).map(Iterator::collect).unwrap_or_default()
}

/// Vectors for every knight move from a position.
const KNIGHT_VECTORS: [(i32, i32); 8] = [
    (1, 2),
    (1, -2),
    (-1, 2),
    (-1, -2),
    (2, 1),
    (-2, 1),
    (2, -1),
    (-2, -1),
];

/// Rook magics for each square.
const ROOK_MAGICS: [BitBoard; 64] = [
    BitBoard(72075735983988992),
    BitBoard(162164771226042368),
    BitBoard(2774234964794286080),
    BitBoard(9295447227374240800),
    BitBoard(7133704077631881220),
    BitBoard(5404321769049293056),
    BitBoard(13871089051341160576),
    BitBoard(4647732546161868928),
    BitBoard(1154188151204364296),
    BitBoard(281623304421378),
    BitBoard(9585349132126560768),
    BitBoard(324399945019818112),
    BitBoard(1266654575591552),
    BitBoard(294422971669283848),
    BitBoard(9228016932324638976),
    BitBoard(422213622698112),
    BitBoard(18019346383143456),
    BitBoard(13519870926790656),
    BitBoard(6917743432679031040),
    BitBoard(4611968593184169992),
    BitBoard(12170978542791720968),
    BitBoard(144159173373870084),
    BitBoard(73228578216739328),
    BitBoard(2199036100765),
    BitBoard(56330731617533952),
    BitBoard(148619063654883328),
    BitBoard(4625232012420055168),
    BitBoard(14988261623278407680),
    BitBoard(1478588125675784194),
    BitBoard(577024260602875912),
    BitBoard(2468254118020653568),
    BitBoard(144256209032118404),
    BitBoard(40577751509369480),
    BitBoard(6917564213158219778),
    BitBoard(9007478444400656),
    BitBoard(20839044434890752),
    BitBoard(4611976300242928640),
    BitBoard(4617878489423415312),
    BitBoard(11278859869620225),
    BitBoard(288230653210657060),
    BitBoard(576531123197214720),
    BitBoard(844699816624161),
    BitBoard(4616198431329755136),
    BitBoard(1513221569692893216),
    BitBoard(12125942013883416584),
    BitBoard(4613005570896036100),
    BitBoard(72066394459734032),
    BitBoard(1765429764459462660),
    BitBoard(342291713626218624),
    BitBoard(22518273021051200),
    BitBoard(9464597434109056),
    BitBoard(613052534176650752),
    BitBoard(20547690614100224),
    BitBoard(140746078552192),
    BitBoard(45044801233552384),
    BitBoard(27028749086179840),
    BitBoard(290556685111457),
    BitBoard(288865903000617090),
    BitBoard(1161084417409045),
    BitBoard(289075918041778209),
    BitBoard(2522578810537804930),
    BitBoard(1298444514277720065),
    BitBoard(1143496522109444),
    BitBoard(2305843716071555138),
];

/// Bishop magics for each square.
const BISHOP_MAGICS: [BitBoard; 64] = [
    BitBoard(1179020146311185),
    BitBoard(145267478427205635),
    BitBoard(4504158111531524),
    BitBoard(9224516499644878888),
    BitBoard(144680405855912002),
    BitBoard(4619005622497574912),
    BitBoard(1130315234418688),
    BitBoard(5349125176573952),
    BitBoard(6071010655858065920),
    BitBoard(20310248111767713),
    BitBoard(1297094009090539520),
    BitBoard(4616233778910625860),
    BitBoard(2305849615159678976),
    BitBoard(74381998193642242),
    BitBoard(1407684255942661),
    BitBoard(2305862803678299144),
    BitBoard(22535635693734016),
    BitBoard(4503608284938884),
    BitBoard(11259016393073153),
    BitBoard(108650578499878976),
    BitBoard(41095363813851170),
    BitBoard(9232520132522148096),
    BitBoard(70385943187776),
    BitBoard(9227035893351617024),
    BitBoard(1155182103739172867),
    BitBoard(11530343153862181120),
    BitBoard(2295791083930624),
    BitBoard(1130297991168512),
    BitBoard(281543712980996),
    BitBoard(307513611096490433),
    BitBoard(2289183226103316),
    BitBoard(4612816874811392128),
    BitBoard(4547891544985604),
    BitBoard(3458958372559659520),
    BitBoard(303473866573824),
    BitBoard(1729558217427519744),
    BitBoard(5633914760597520),
    BitBoard(1441434463836899328),
    BitBoard(20269028707403544),
    BitBoard(149744981853258752),
    BitBoard(2252933819802113),
    BitBoard(1163074498090533888),
    BitBoard(4681729134575680),
    BitBoard(4621485970984798208),
    BitBoard(367078571518203970),
    BitBoard(72621098075685120),
    BitBoard(1225544256278495744),
    BitBoard(1411779381045761),
    BitBoard(5333500077688291352),
    BitBoard(4716913491968128),
    BitBoard(148627764202701056),
    BitBoard(1688850967695425),
    BitBoard(17781710002178),
    BitBoard(9243644149415084036),
    BitBoard(218426849703891488),
    BitBoard(9009415596316677),
    BitBoard(1412882374067224),
    BitBoard(279186509824),
    BitBoard(20407489916899328),
    BitBoard(4614113755159331840),
    BitBoard(144119586390940160),
    BitBoard(11547234118442230016),
    BitBoard(5188151323463779840),
    BitBoard(435758450535334272),
];
