//! Modules that enable the core game features, such as Board representation
//! and Move generation.

pub mod bitboard;
pub mod board;
pub mod castle;
pub mod color;
pub mod fen;
pub mod magics;
pub mod move_gen;
pub mod move_list;
pub mod moves;
pub mod piece;
pub mod square;
pub mod zobrist;
