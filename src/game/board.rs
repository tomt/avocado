//! Board representation.

use crate::{
    error::{Error, Result},
    game::{
        bitboard::BitBoard,
        castle::CastleRights,
        castle::CastleSide,
        color::Color,
        fen::Fen,
        move_gen::MoveGen,
        move_list::MoveList,
        moves::{Move, MoveType},
        piece::Piece,
        square::Square,
        zobrist::Zobrist,
    },
};
use std::{
    convert::TryFrom,
    fmt::{Display, Formatter},
    ops::Index,
};

/// Represents the possible states of the game.
pub enum GameState {
    /// Ongoing game, with `.0` to play
    ToPlay(Color),
    /// Color `.0` has won
    Winner(Color),
    /// Draw by 50 move rule
    Draw50,
    /// Stalemate
    Stalemate,
    /// Draw by threefold repetition
    ThreefoldRepetition,
    /// Draw by low material.
    LowMaterial,
}

/// Represents a chess board. This implementation uses
/// both bitboards and piece-square representation.
#[derive(PartialEq, Clone, Debug)]
pub struct Board {
    /// The current player / color
    to_play: Color,

    /// The count of moves for while no pawn has moved and no piece
    /// has been captured. Used for draw by 50 move rule.
    fifty_move: usize,

    /// The number of full moves (both players) that have elapsed
    full_move_count: usize,

    /// The number of half moves (one player) that have elapsed
    half_move_count: usize,

    /// The castling rights for white and black
    rights: CastleRights,

    /// The square where an en passant capture may take place,
    /// None if there is no such square.
    en_passant: Option<Square>,

    /// The piece-square representation of the board
    pieces: [Option<Piece>; 64],

    /// One bitboard for each white & black piece
    bb_pieces: [BitBoard; 12],

    /// One bitboard for all white pieces,
    /// one for all black pieces
    bb_combined: [BitBoard; 2],

    /// The current zobrist hash.
    zobrist: Zobrist,

    /// The stack of zobrist hashes.
    zobrist_stack: Vec<BitBoard>,
}

impl Default for Board {
    fn default() -> Board {
        Board::new("rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1").unwrap()
    }
}
impl Display for Board {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        writeln!(f, "    a b c d e f g h\n  ╭─────────────────╮")?;

        for rank in 0..8 {
            write!(f, "{} │", 8 - rank)?;
            for file in 0..8 {
                match self[(rank, file)] {
                    Some(piece) => write!(f, " {piece}")?,
                    None => write!(f, "  ")?,
                }
            }
            writeln!(f, " │ {}", 8 - rank)?;
        }

        writeln!(
            f,
            "  ╰─────────────────╯\n    a b c d e f g h\n       {} to move",
            self.to_play
        )
    }
}

impl<T: Into<Square>> Index<T> for Board {
    type Output = Option<Piece>;

    fn index(&self, index: T) -> &Self::Output {
        &self.pieces[usize::from(index.into())]
    }
}

impl Board {
    /// Creates a new [`Board`] from `fen` (Forsyth-Edwards Notation).
    pub fn new(fen: &str) -> Result<Self> {
        if fen.is_empty() {
            return Err(Error::FenEmpty);
        }

        let mut board = Board {
            to_play: Color::White,
            fifty_move: 0,
            full_move_count: 0,
            half_move_count: 0,
            rights: CastleRights::default(),
            en_passant: None,
            pieces: [None; 64],
            bb_pieces: [BitBoard::EMPTY; 12],
            bb_combined: [BitBoard::EMPTY; 2],
            zobrist: Zobrist::default(),
            zobrist_stack: vec![],
        };

        // split args
        let args = fen.split_whitespace().collect::<Vec<_>>();
        if args.len() != 6 {
            return Err(Error::FenArgsLen(args.len()));
        }

        // parse pieces
        let mut pos = 0;

        for ch in args[0].chars() {
            let piece = match ch {
                'p' => Piece::BlackPawn,
                'n' => Piece::BlackKnight,
                'b' => Piece::BlackBishop,
                'r' => Piece::BlackRook,
                'q' => Piece::BlackQueen,
                'k' => Piece::BlackKing,

                'P' => Piece::WhitePawn,
                'N' => Piece::WhiteKnight,
                'B' => Piece::WhiteBishop,
                'R' => Piece::WhiteRook,
                'Q' => Piece::WhiteQueen,
                'K' => Piece::WhiteKing,

                '1'..='8' => {
                    pos += ch.to_digit(10).unwrap() as usize;
                    continue;
                }

                '/' => continue,

                _ => return Err(Error::FenBadChar(ch)),
            };

            board.bb_mut(piece).set(pos);
            board.pieces[pos] = Some(piece);

            pos += 1;

            if pos >= 64 {
                break;
            }
        }

        if pos != 64 {
            return Err(Error::FenPiecesLen(pos));
        }

        board.to_play = Color::try_from(args[1])?;
        board.rights = CastleRights::try_from(args[2])?;
        board.en_passant = match args[3] {
            "-" => None,
            v => Some(Square::try_from(v)?),
        };
        board.fifty_move = args[4].parse().map_err(Error::FenParseInt)?;
        board.full_move_count = args[5].parse().map_err(Error::FenParseInt)?;

        // init zobrist hash
        for (sq, piece) in Square::iter().zip(board.pieces.iter()) {
            if let &Some(piece) = piece {
                board.zobrist.hash(sq, piece);
            }
        }
        board.zobrist_stack.push(board.zobrist.key());

        // compute combined bitboards
        Color::iter().for_each(|color| {
            *board.bb_all_mut(color) = board.bb(Piece::pawn(color))
                | board.bb(Piece::knight(color))
                | board.bb(Piece::bishop(color))
                | board.bb(Piece::rook(color))
                | board.bb(Piece::queen(color))
                | board.bb(Piece::king(color))
        });

        Ok(board)
    }

    /// Mutably borrows the piece at `sq`.
    #[inline(always)]
    fn at_mut(&mut self, sq: Square) -> &mut Option<Piece> {
        &mut self.pieces[usize::from(sq)]
    }
    /// Gets the bitboard for `piece`.
    #[inline(always)]
    pub fn bb(&self, piece: Piece) -> BitBoard {
        self.bb_pieces[usize::from(piece)]
    }
    /// Gets the combined bitboard for `color`.
    #[inline(always)]
    pub fn bb_all(&self, color: Color) -> BitBoard {
        self.bb_combined[usize::from(color)]
    }
    /// Mutably borrows the bitboard for `piece`.
    fn bb_mut(&mut self, piece: Piece) -> &mut BitBoard {
        &mut self.bb_pieces[usize::from(piece)]
    }
    /// Mutably borrows the combined bitboard for `color`.
    fn bb_all_mut(&mut self, color: Color) -> &mut BitBoard {
        &mut self.bb_combined[usize::from(color)]
    }
    /// Gets a bitboard containing all occupied squares.
    #[inline(always)]
    pub fn bb_occupied(&self) -> BitBoard {
        self.bb_combined[0] | self.bb_combined[1]
    }

    /// Gets the current player.
    pub fn to_play(&self) -> Color {
        self.to_play
    }
    /// Gets the castle rights.
    pub fn rights(&self) -> &CastleRights {
        &self.rights
    }
    /// Gets the en passant square.
    pub fn en_passant(&self) -> Option<Square> {
        self.en_passant
    }

    /// Gets the fifty move count.
    pub fn fifty_move(&self) -> usize {
        self.fifty_move
    }
    /// Gets the full move count.
    pub fn full_move_count(&self) -> usize {
        self.full_move_count
    }
    /// Gets the half move count.
    pub fn half_move_count(&self) -> usize {
        self.half_move_count
    }

    /// Gets the zobrist hash for the current position.
    pub fn zobrist(&self) -> BitBoard {
        self.zobrist.key()
    }
    /// Gets the FEN (Forsyth-Edwards Notation) for the current
    /// position.
    pub fn fen(&self) -> Fen<'_> {
        Fen::new(self)
    }

    /// Moves a piece from `start` to `end`, allowing specifying
    /// a different piece for the end square. Automatically updates
    /// the zobrist hash.
    fn move_se(&mut self, start: Square, end: Square, start_piece: Piece, end_piece: Piece) {
        self.bb_mut(start_piece).clear(start);
        self.bb_mut(end_piece).set(end);
        self.bb_all_mut(start_piece.color()).clear(start);
        self.bb_all_mut(end_piece.color()).set(end);
        *self.at_mut(start) = None;
        *self.at_mut(end) = Some(end_piece);

        self.zobrist.hash(start, start_piece).hash(end, end_piece);
    }
    /// Moves a piece from `start` to `end`. Updates the zobrist hash.
    fn move_piece(&mut self, start: Square, end: Square, piece: Piece) {
        self.move_se(start, end, piece, piece);
    }
    /// Captures the piece at square `pos` (if any). Updates the zobrist hash.
    fn capture_piece(&mut self, sq: Square) {
        if let Some(piece) = self[sq] {
            debug_assert!(piece.color() != self.to_play);

            self.bb_mut(piece).clear(sq);
            self.bb_all_mut(piece.color()).clear(sq);
            *self.at_mut(sq) = None;

            if piece.is_rook() {
                self.rights.disable_for_sq(sq);
            }

            self.zobrist.hash(sq, piece);
        }
    }

    /// Reverses moving a piece from `start` to `end`.
    fn unmove_se(&mut self, start: Square, end: Square, start_piece: Piece, end_piece: Piece) {
        self.bb_mut(start_piece).set(start);
        self.bb_mut(end_piece).clear(end);
        self.bb_all_mut(start_piece.color()).set(start);
        self.bb_all_mut(end_piece.color()).clear(end);
        *self.at_mut(start) = Some(start_piece);
        *self.at_mut(end) = None;
    }
    /// Reverses moving a piece.
    fn unmove_piece(&mut self, start: Square, end: Square, piece: Piece) {
        self.unmove_se(start, end, piece, piece);
    }
    /// Reverses the capture of a piece.
    fn uncapture_piece(&mut self, sq: Square, piece: Piece) {
        self.bb_mut(piece).set(sq);
        self.bb_all_mut(piece.color()).set(sq);
        *self.at_mut(sq) = Some(piece);
    }

    /// Makes the move `mv` on the board.
    pub fn make_move(&mut self, mv: Move, info: &mut UndoInfo) {
        info.rights = self.rights;
        info.en_passant = self.en_passant;
        info.fifty_move = self.fifty_move;

        // limited update for null moves.
        if mv == Move::null() {
            self.en_passant = None;
            self.fifty_move += 1;
            self.half_move_count += 1;
            self.to_play = !self.to_play;
            return;
        }

        // load data from move
        let start = mv.start();
        let end = mv.end();

        // store start and end pieces
        let start_piece = self[start].unwrap();
        debug_assert!(start != end);
        debug_assert!(start_piece.color() == self.to_play);

        // store captured piece.
        info.cap = self[end];

        self.fifty_move = match start_piece.is_pawn() || self[end].is_some() {
            true => 0,
            false => self.fifty_move + 1,
        };

        match mv.move_type() {
            MoveType::EnPassant => {
                self.capture_piece(end);
                self.move_piece(start, self.en_passant.unwrap(), Piece::pawn(self.to_play));
            }
            MoveType::Castle => {
                let offset = Square::from(start as usize & 0b111000);
                let (r_start, r_end) = match mv.castle_side() {
                    CastleSide::KingSide => (offset + 7, offset + 5),
                    CastleSide::QueenSide => (offset, offset + 3),
                };

                debug_assert!(self.rights.can_castle(self.to_play, mv.castle_side()));
                debug_assert!(self[r_start].unwrap().is_rook());

                self.move_piece(start, end, Piece::king(self.to_play));
                self.move_piece(r_start, r_end, Piece::rook(self.to_play));

                self.rights.disable_for_color(self.to_play);
            }
            MoveType::Promotion => {
                debug_assert!(start_piece.is_pawn());

                self.capture_piece(end);
                self.move_se(
                    start,
                    end,
                    Piece::pawn(self.to_play),
                    mv.promotion_piece().to_piece(self.to_play),
                );
            }
            MoveType::Standard => {
                self.capture_piece(end);

                if start_piece.is_king() {
                    self.rights.disable_for_color(self.to_play);
                } else if start_piece.is_rook() {
                    self.rights.disable_for_sq(start);
                }

                self.move_piece(start, end, start_piece);
            }
        }

        self.en_passant = match start_piece.is_pawn() && end.diff(start) == 16 {
            true => Some(start.mid(end)),
            false => None,
        };
        self.half_move_count += 1;
        self.to_play = !self.to_play;
        self.zobrist_stack.push(self.zobrist.key());
    }
    /// Undoes the previous move on the board.
    pub fn undo_move(&mut self, mv: Move, info: &UndoInfo) {
        // handle reversible changes
        self.to_play = !self.to_play;
        self.half_move_count -= 1;

        // load non-reversible changes from info.
        self.fifty_move = info.fifty_move;
        self.rights = info.rights;
        self.en_passant = info.en_passant;

        // early return for null moves.
        if mv == Move::null() {
            return;
        }

        // update zobrist hash.
        self.zobrist_stack.pop();
        self.zobrist
            .set(self.zobrist_stack[self.zobrist_stack.len() - 1]);

        // load data from move
        let start = mv.start();
        let end = mv.end();
        debug_assert!(start != end);

        // update bitboards & piece-square table.
        match mv.move_type() {
            MoveType::EnPassant => {
                self.unmove_piece(start, self.en_passant.unwrap(), Piece::pawn(self.to_play));
                self.uncapture_piece(end, info.cap.unwrap());
            }
            MoveType::Castle => {
                let offset = Square::from(start as usize & 0b111000);
                let (r_start, r_end) = match mv.castle_side() {
                    CastleSide::KingSide => (offset + 7, offset + 5),
                    CastleSide::QueenSide => (offset, offset + 3),
                };
                self.unmove_piece(r_start, r_end, Piece::rook(self.to_play));
                self.unmove_piece(start, end, Piece::king(self.to_play));
            }
            MoveType::Promotion => {
                self.unmove_se(
                    start,
                    end,
                    Piece::pawn(self.to_play),
                    mv.promotion_piece().to_piece(self.to_play),
                );
                if let Some(cap) = info.cap {
                    self.uncapture_piece(end, cap);
                }
            }
            MoveType::Standard => {
                let start_piece = self[end].unwrap();
                self.unmove_piece(start, end, start_piece);
                if let Some(cap) = info.cap {
                    self.uncapture_piece(end, cap);
                }
            }
        }
    }

    /// Checks whether the position is a draw by threefold repetition
    fn is_threefold_repetition(&self) -> bool {
        self.zobrist_stack
            .iter()
            .filter(|&&h| h == self.zobrist())
            .count()
            >= 3
    }
    /// Checks whether the position is a draw by low material
    fn is_low_material(&self) -> bool {
        let white_count = self.bb_all(Color::White).count_ones();
        let black_count = self.bb_all(Color::Black).count_ones();

        match (white_count, black_count) {
            // w/king vs b/king.
            (1, 1) => true,
            // w/king vs b/king + b/bishop
            (1, 2) if !self.bb(Piece::BlackBishop).is_empty() => true,
            // w/king vs b/king + b/knight
            (1, 2) if !self.bb(Piece::BlackKnight).is_empty() => true,
            // b/king vs w/king + w/bishop
            (2, 1) if !self.bb(Piece::WhiteBishop).is_empty() => true,
            // b/king vs w/king + w/knight
            (2, 1) if !self.bb(Piece::WhiteKnight).is_empty() => true,
            // w/king + w/bishop vs b/king + b/bishop if
            (2, 2) => {
                let bishops = self.bb(Piece::WhiteBishop) | self.bb(Piece::BlackBishop);

                // must be two bishops, and on same color.
                bishops.count_ones() == 2 && (bishops == bishops & BitBoard::WHITE_SQ)
                    || (bishops == bishops & BitBoard::BLACK_SQ)
            }
            _ => false,
        }
    }
    /// Checks whether the position is a fifty move draw
    fn is_fifty_move_draw(&self) -> bool {
        self.fifty_move >= 100
    }
    /// Checks whether any of the draw conditions have occured
    pub fn is_draw(&self) -> bool {
        self.is_fifty_move_draw() || self.is_threefold_repetition() || self.is_low_material()
    }
    /// Gets the [`GameState`] corresponding to the board position.
    pub fn game_state(&self, gen: &MoveGen) -> GameState {
        if self.is_fifty_move_draw() {
            GameState::Draw50
        } else if self.is_threefold_repetition() {
            GameState::ThreefoldRepetition
        } else if self.is_low_material() {
            GameState::LowMaterial
        } else {
            // find all moves
            let mut move_list = MoveList::default();
            gen.gen_moves(&mut move_list, self);

            match (move_list.len(), gen.is_in_check(self)) {
                (0, true) => GameState::Winner(!self.to_play),
                (0, false) => GameState::Stalemate,
                _ => GameState::ToPlay(self.to_play),
            }
        }
    }
    /// Gets the position of the friendly king.
    pub fn king_pos(&self) -> Square {
        self.bb(Piece::king(self.to_play)).lsb_idx()
    }
}

/// Stores the information required to undo a move.
#[derive(Debug, Default)]
pub struct UndoInfo {
    fifty_move: usize,
    rights: CastleRights,
    en_passant: Option<Square>,
    cap: Option<Piece>,
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn fen() {
        macro_rules! fen_test {
            ($arg:tt) => {
                let board = Board::new($arg).unwrap();
                assert_eq!(board.fen().to_string(), $arg, "\n{}", board);
            };
        }

        fen_test!("8/8/8/2k5/2pP4/8/B7/4K3 b - d3 5 3");
        fen_test!("r6r/1b2k1bq/8/8/7B/8/8/R3K2R b QK - 3 2");
        fen_test!("r1bqkbnr/pppppppp/n7/8/8/P7/1PPPPPPP/RNBQKBNR w QqKk - 2 2");
        fen_test!("r3k2r/p1pp1pb1/bn2Qnp1/2qPN3/1p2P3/2N5/PPPBBPPP/R3K2R b QqKk - 3 2");
        fen_test!("rnb2k1r/pp1Pbppp/2p5/q7/2B5/8/PPPQNnPP/RNB1K2R w QK - 3 9");
        fen_test!("2r5/3pk3/8/2P5/8/2K5/8/8 w - - 5 4");
        fen_test!("2kr3r/p1ppqpb1/bn2Qnp1/3PN3/1p2P3/2N5/PPPBBPPP/R3K2R b QK - 3 2");
        fen_test!("4k3/8/8/5R2/8/8/8/4K3 b - - 0 1");
        fen_test!("8/4k3/8/8/4R3/8/8/4K3 b - - 0 1");
        fen_test!("4k3/6N1/5b2/4R3/8/8/8/4K3 b - - 0 1");
        fen_test!("4k3/8/6n1/4R3/8/8/8/4K3 b - - 0 1");
        fen_test!("8/8/8/2k5/3Pp3/8/8/4K3 b - d3 0 1");
        fen_test!("8/8/8/1k6/3Pp3/8/8/4KQ2 b - d3 0 1");
        fen_test!("4k3/8/4r3/8/8/4Q3/8/2K5 b - - 0 1");
        fen_test!("8/8/8/8/k2Pp2Q/8/8/2K5 b - d3 0 1");
    }
}
