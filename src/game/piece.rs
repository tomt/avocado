//! Piece representation

use crate::game::color::Color;
use std::fmt::{Display, Formatter};

/// Represents a piece and its color.
#[derive(Clone, Copy, Debug, PartialEq, Eq)]
#[allow(missing_docs)]
pub enum Piece {
    WhitePawn = 0,
    WhiteKnight = 1,
    WhiteBishop = 2,
    WhiteRook = 3,
    WhiteQueen = 4,
    WhiteKing = 5,
    BlackPawn = 6,
    BlackKnight = 7,
    BlackBishop = 8,
    BlackRook = 9,
    BlackQueen = 10,
    BlackKing = 11,
}
impl Piece {
    /// Gets the [`Color`] of the [`Piece`].
    pub fn color(&self) -> Color {
        match *self as usize {
            0..=5 => Color::White,
            _ => Color::Black,
        }
    }

    /// Is the piece a pawn?
    pub fn is_pawn(&self) -> bool {
        matches!(*self, Piece::WhitePawn | Piece::BlackPawn)
    }
    /// Is the piece a knight?
    pub fn is_knight(&self) -> bool {
        matches!(*self, Piece::WhiteKnight | Piece::BlackKnight)
    }
    /// Is the piece a bishop?
    pub fn is_bishop(&self) -> bool {
        matches!(*self, Piece::WhiteBishop | Piece::BlackBishop)
    }
    /// Is the piece a rook?
    pub fn is_rook(&self) -> bool {
        matches!(*self, Piece::WhiteRook | Piece::BlackRook)
    }
    /// Is the piece a queen?
    pub fn is_queen(&self) -> bool {
        matches!(*self, Piece::WhiteQueen | Piece::BlackQueen)
    }
    /// Is the piece a king?
    pub fn is_king(&self) -> bool {
        matches!(*self, Piece::WhiteKing | Piece::BlackKing)
    }

    /// Create a pawn with `color`.
    pub fn pawn(color: Color) -> Piece {
        match color {
            Color::White => Piece::WhitePawn,
            Color::Black => Piece::BlackPawn,
        }
    }
    /// Create a knight with `color`.
    pub fn knight(color: Color) -> Piece {
        match color {
            Color::White => Piece::WhiteKnight,
            Color::Black => Piece::BlackKnight,
        }
    }
    /// Create a bishop with `color`.
    pub fn bishop(color: Color) -> Piece {
        match color {
            Color::White => Piece::WhiteBishop,
            Color::Black => Piece::BlackBishop,
        }
    }
    /// Create a rook with `color`.
    pub fn rook(color: Color) -> Piece {
        match color {
            Color::White => Piece::WhiteRook,
            Color::Black => Piece::BlackRook,
        }
    }
    /// Create a queen with `color`.
    pub fn queen(color: Color) -> Piece {
        match color {
            Color::White => Piece::WhiteQueen,
            Color::Black => Piece::BlackQueen,
        }
    }
    /// Create a king with `color`.
    pub fn king(color: Color) -> Piece {
        match color {
            Color::White => Piece::WhiteKing,
            Color::Black => Piece::BlackKing,
        }
    }
}
impl Display for Piece {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        const NOTATION: &str = "PNBRQKpnbrqk";
        write!(f, "{}", NOTATION.chars().nth(*self as usize).unwrap())
    }
}
impl From<Piece> for usize {
    fn from(piece: Piece) -> Self {
        piece as usize
    }
}
