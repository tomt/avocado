//! Bitset with 64 values to represent a bitboard.
//!
//!  a8 => lsb (index 0)
//!  h1 => msb (index 63)
//!
//! ```md,ignore
//!     a b c d e f g h    
//!   ╭─────────────────╮  
//! 8 │ x x x x x x x x │ 8
//! 7 │ x x x x x x x x │ 7
//! 6 │ x x x x x x x x │ 6
//! 5 │ x x x x x x x x │ 5
//! 4 │ x x x x x x x x │ 4
//! 3 │ x x x x x x x x │ 3
//! 2 │ x x x x x x x x │ 2
//! 1 │ x x x x x x x x │ 1
//!   ╰─────────────────╯  
//!     a b c d e f g h    
//! ```

use super::square::Square::{self, *};
use std::{
    cmp::PartialEq,
    fmt::Display,
    ops::{
        Add, AddAssign, BitAnd, BitAndAssign, BitOr, BitOrAssign, BitXor, BitXorAssign, Deref, Mul,
        MulAssign, Not, Shl, ShlAssign, Shr, ShrAssign, Sub, SubAssign,
    },
};

#[rustfmt::skip]
const LSB_64_TABLE: [Square; 64] = [
    H1, G5, D8, A4, B5, B3, G6, B4, 
    H7, C2, C3, F7, D7, F2, D6, C4, 
    F1, F5, C8, D2, F6, D3, F3, C7, 
    C6, H3, B8, G2, B7, B1, A8, D4, 
    G1, H5, A3, E8, B2, F8, E2, C5, 
    E1, G8, H6, E3, G3, D5, A1, A6, 
    H8, H4, A2, A5, D1, G7, E7, H2, 
    G4, E5, C1, E6, F4, B6, E4, A7,
];

/// Used to implement bitboard operations on a u64
#[repr(transparent)]
#[derive(Debug, Default, Clone, Copy, PartialEq, Eq)]
pub struct BitBoard(pub u64);
impl BitBoard {
    /// An empty bitboard.
    pub const EMPTY: BitBoard = BitBoard(0);
    /// A full bitboard.
    pub const FULL: BitBoard = BitBoard(u64::MAX);

    /// Bitboard of the white squares.
    pub const WHITE_SQ: BitBoard = BitBoard(0xaa55_aa55_aa55_aa55);
    /// Bitboard of the black squares.
    pub const BLACK_SQ: BitBoard = BitBoard(0x55aa_55aa_55aa_55aa);

    /// Removes the least significant bit from the bitboard,
    /// returning its position.
    #[inline(always)]
    pub fn pop_lsb(&mut self) -> Square {
        let idx = self.lsb_idx();
        *self &= *self - 1;
        idx
    }
    /// Gets the position of the least significant bit.
    #[inline(always)]
    pub fn lsb_idx(&self) -> Square {
        debug_assert!(!self.is_empty());
        let b = *self ^ (*self - 1);
        let folded = (b & 0xff_ff_ff_ff) ^ (b >> 32);
        let idx = usize::from((folded * 0x78_3a_9b_23) >> 26);
        LSB_64_TABLE[idx & 0b111111]
    }
    /// Sets the bit at position `idx` to 1.
    #[inline(always)]
    pub fn set(&mut self, sq: impl Into<Square>) -> &mut Self {
        *self |= BitBoard::from(sq);
        self
    }
    /// Sets the bit at position `idx` to 0.
    #[inline(always)]
    pub fn clear(&mut self, sq: impl Into<Square>) -> &mut Self {
        *self &= !BitBoard::from(sq);
        self
    }
    /// Checks whether the bit at `idx` is set to 1.
    #[inline(always)]
    pub fn contains(&self, sq: impl Into<Square>) -> bool {
        !(*self & BitBoard::from(sq)).is_empty()
    }
    /// Checks whether the bitboard is empty.
    #[inline(always)]
    pub fn is_empty(self) -> bool {
        self == BitBoard(0)
    }
    /// Reverses the bits.
    pub const fn reverse(self) -> Self {
        Self(self.0.reverse_bits())
    }
}
impl Display for BitBoard {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        writeln!(f, "    a b c d e f g h\n  ╭─────────────────╮")?;

        for rank in 0..8 {
            write!(f, "{} │ ", 8 - rank)?;

            for file in 0..8 {
                match self.contains((rank, file)) {
                    true => write!(f, "x ")?,
                    false => write!(f, ". ")?,
                }
            }

            writeln!(f, "│ {}", 8 - rank)?;
        }

        write!(f, "  ╰─────────────────╯\n    a b c d e f g h")
    }
}

impl<T: Into<Square>> From<T> for BitBoard {
    #[inline(always)]
    fn from(val: T) -> Self {
        let idx = usize::from(val.into());
        BitBoard(1 << idx)
    }
}
impl From<BitBoard> for usize {
    #[inline(always)]
    fn from(BitBoard(bb): BitBoard) -> Self {
        bb as usize
    }
}
impl From<BitBoard> for u64 {
    #[inline(always)]
    fn from(BitBoard(bb): BitBoard) -> Self {
        bb as u64
    }
}
impl Deref for BitBoard {
    type Target = u64;

    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl Not for BitBoard {
    type Output = Self;

    fn not(self) -> Self::Output {
        BitBoard(!self.0)
    }
}

macro_rules! impl_op {
    ($_trait:ident, $_method:ident) => {
        impl $_trait<u64> for BitBoard {
            type Output = BitBoard;
            fn $_method(self, bb_b: u64) -> BitBoard {
                let BitBoard(bb_a) = self;
                BitBoard(bb_a.$_method(bb_b))
            }
        }
        impl $_trait<BitBoard> for BitBoard {
            type Output = BitBoard;
            fn $_method(self, BitBoard(bb_b): BitBoard) -> BitBoard {
                let BitBoard(bb_a) = self;
                BitBoard(bb_a.$_method(bb_b))
            }
        }
    };
    ($_trait:ident, $_trait_assign:ident, $_method:ident, $_method_assign:ident) => {
        impl_op!($_trait, $_method);

        impl $_trait_assign<BitBoard> for BitBoard {
            fn $_method_assign(&mut self, BitBoard(bb_b): BitBoard) {
                let BitBoard(ref mut bb_a) = self;
                *bb_a = bb_a.$_method(bb_b);
            }
        }
        impl $_trait_assign<u64> for BitBoard {
            fn $_method_assign(&mut self, bb_b: u64) {
                let BitBoard(ref mut bb_a) = self;
                *bb_a = bb_a.$_method(bb_b);
            }
        }
    };
}

impl_op!(Add, AddAssign, add, add_assign);
impl_op!(Sub, SubAssign, sub, sub_assign);
impl_op!(Mul, MulAssign, mul, mul_assign);
impl_op!(Shl, ShlAssign, shl, shl_assign);
impl_op!(Shr, ShrAssign, shr, shr_assign);
impl_op!(BitOr, BitOrAssign, bitor, bitor_assign);
impl_op!(BitXor, BitXorAssign, bitxor, bitxor_assign);
impl_op!(BitAnd, BitAndAssign, bitand, bitand_assign);

impl FromIterator<Square> for BitBoard {
    fn from_iter<T: IntoIterator<Item = Square>>(iter: T) -> Self {
        iter.into_iter().fold(BitBoard::EMPTY, |mut acc, sq| {
            acc.set(sq);
            acc
        })
    }
}
impl IntoIterator for BitBoard {
    type Item = Square;
    type IntoIter = BitBoardIntoIter;

    fn into_iter(self) -> Self::IntoIter {
        BitBoardIntoIter(self)
    }
}

/// Iterates over the squares on a bitboard.
pub struct BitBoardIntoIter(BitBoard);
impl Iterator for BitBoardIntoIter {
    type Item = Square;

    #[inline(always)]
    fn next(&mut self) -> Option<Self::Item> {
        let BitBoardIntoIter(bb) = self;

        match bb.is_empty() {
            true => None,
            false => Some(bb.pop_lsb()),
        }
    }
}
