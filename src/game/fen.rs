//! Formats a board using FEN.

use crate::game::{board::Board, square::Square};
use std::fmt;

/// Wraps `Board`, providing a `Display` impl that writes
/// the Forsyth-Edwards notation string.
pub struct Fen<'a> {
    board: &'a Board,
}
impl<'a> Fen<'a> {
    /// Creates a Fen wrapper around a board.
    pub fn new(board: &'a Board) -> Self {
        Self { board }
    }
}
impl<'a> fmt::Display for Fen<'a> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let mut no_empty = 0;

        for sq in Square::iter() {
            match (no_empty, self.board[sq]) {
                (_, None) => no_empty += 1,
                (n, Some(piece)) => {
                    no_empty = 0;
                    match n {
                        0 => write!(f, "{piece}")?,
                        n => write!(f, "{n}{piece}")?,
                    }
                }
            }
            let (rank, file) = sq.rf();
            if sq != Square::A1 && file == 7 {
                if no_empty > 0 {
                    write!(f, "{no_empty}")?;
                }
                if rank < 7 {
                    write!(f, "/")?;
                }
                no_empty = 0;
            }
        }

        write!(
            f,
            " {} {} {} {} {}",
            self.board.to_play(),
            self.board.rights(),
            match self.board.en_passant() {
                Some(sq) => sq.to_string(),
                _ => "-".to_owned(),
            },
            self.board.fifty_move(),
            self.board.full_move_count(),
        )
    }
}
