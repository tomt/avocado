//! Representing castling rights.

use crate::{
    error::{Error, Result},
    game::{color::Color, square::Square},
};
use std::{convert::TryFrom, fmt::Display};

/// The side of the board where castling occurs
#[derive(Clone, Copy)]
pub enum CastleSide {
    /// The right side (white perspective)
    QueenSide = 0,
    /// The left side (white perspective)
    KingSide = 1,
}

/// Stores the castling rights for white and black
#[derive(PartialEq, Eq, Clone, Copy, Debug)]
pub struct CastleRights {
    w_ks: bool,
    w_qs: bool,
    b_ks: bool,
    b_qs: bool,
}
impl CastleRights {
    /// Can the player `color` castle on `side` ?
    pub fn can_castle(&self, color: Color, side: CastleSide) -> bool {
        match (color, side) {
            (Color::White, CastleSide::QueenSide) => self.w_qs,
            (Color::White, CastleSide::KingSide) => self.w_ks,
            (Color::Black, CastleSide::QueenSide) => self.b_qs,
            (Color::Black, CastleSide::KingSide) => self.b_ks,
        }
    }
    /// Are all of the castle rights set to false?
    pub fn is_empty(&self) -> bool {
        !(self.b_ks || self.b_qs || self.w_ks || self.w_qs)
    }

    /// Creates a new [`CastleRights`] instance with all castling
    /// rights set to false.
    pub const fn empty() -> Self {
        Self {
            w_ks: false,
            w_qs: false,
            b_ks: false,
            b_qs: false,
        }
    }

    /// Disables the castle rights given that a rook at `sq`
    /// has moved or captured.
    pub fn disable_for_sq(&mut self, sq: Square) {
        match sq {
            Square::A1 => self.w_qs = false,
            Square::H1 => self.w_ks = false,
            Square::A8 => self.b_qs = false,
            Square::H8 => self.b_ks = false,
            _ => (),
        };
    }
    /// Disables castling for `color`
    pub fn disable_for_color(&mut self, color: Color) {
        match color {
            Color::White => {
                self.w_qs = false;
                self.w_ks = false;
            }
            Color::Black => {
                self.b_qs = false;
                self.b_ks = false;
            }
        }
    }
}
impl Default for CastleRights {
    fn default() -> Self {
        Self {
            w_ks: true,
            w_qs: true,
            b_ks: true,
            b_qs: true,
        }
    }
}
impl Display for CastleRights {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self.is_empty() {
            true => write!(f, "-"),
            false => {
                if self.w_qs {
                    write!(f, "Q")?;
                }
                if self.b_qs {
                    write!(f, "q")?;
                }
                if self.w_ks {
                    write!(f, "K")?;
                }
                if self.b_ks {
                    write!(f, "k")?;
                }

                Ok(())
            }
        }
    }
}
impl TryFrom<&str> for CastleRights {
    type Error = Error;

    fn try_from(value: &str) -> Result<Self> {
        let mut rights = CastleRights::empty();

        for ch in value.chars() {
            match ch {
                'Q' => rights.w_qs = true,
                'K' => rights.w_ks = true,
                'q' => rights.b_qs = true,
                'k' => rights.b_ks = true,
                '-' => break,
                _ => return Err(Error::FenCastleBadChar(ch)),
            };
        }

        Ok(rights)
    }
}
