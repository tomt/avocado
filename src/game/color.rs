//! White or Black.

use crate::error::{Error, Result};
use std::{
    convert::TryFrom,
    fmt::{Display, Formatter},
    ops::Not,
};

/// White or Black.
#[derive(PartialEq, Eq, Clone, Copy, Debug)]
pub enum Color {
    /// White player.
    White = 0,
    /// Black player.
    Black = 1,
}
impl Color {
    /// Is the color white?
    pub const fn is_white(&self) -> bool {
        matches!(self, Color::White)
    }
    /// Is the color black?
    pub const fn is_black(&self) -> bool {
        matches!(self, Color::Black)
    }
    /// Flips the color.
    pub const fn enemy(self) -> Self {
        match self {
            Color::White => Color::Black,
            Color::Black => Color::White,
        }
    }
    /// Gets an iterator over the colors.
    pub fn iter() -> impl Iterator<Item = Color> {
        [Color::White, Color::Black].iter().copied()
    }
}
impl From<Color> for usize {
    fn from(color: Color) -> Self {
        color as usize
    }
}
impl Not for Color {
    type Output = Self;

    fn not(self) -> Self::Output {
        self.enemy()
    }
}
impl Display for Color {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        match *self {
            Color::White => write!(f, "w"),
            Color::Black => write!(f, "b"),
        }
    }
}
impl From<usize> for Color {
    fn from(val: usize) -> Self {
        match val {
            0 => Color::White,
            _ => Color::Black,
        }
    }
}
impl TryFrom<&str> for Color {
    type Error = Error;

    fn try_from(value: &str) -> Result<Self> {
        match value.to_lowercase().as_ref() {
            "w" | "white" => Ok(Color::White),
            "b" | "black" => Ok(Color::Black),
            _ => Err(Error::FenParseColor(value.to_owned())),
        }
    }
}
