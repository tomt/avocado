//! Represents a Square on the Board

use crate::error::{Error, Result};
use std::{
    convert::TryFrom,
    fmt::Display,
    ops::{Add, Sub},
};

/// Represents a position on the Board.  
/// Tiles are numbered from A8 = 0 to H1 = 63.
///
/// (0)                         (7)
/// A8, B8, C8, D8, E8, F8, G8, H8,
/// A7, B7, C7, D7, E7, F7, G7, H7,
/// A6, B6, C6, D6, E6, F6, G6, H6,
/// A5, B5, C5, D5, E5, F5, G5, H5,
/// A4, B4, C4, D4, E4, F4, G4, H4,
/// A3, B3, C3, D3, E3, F3, G3, H3,
/// A2, B2, C2, D2, E2, F2, G2, H2,
/// A1, B1, C1, D1, E1, F1, G1, H1,
/// (56)                       (63)
/// 
/// Rank and file do not use traditional chess numbering,
/// instead using a coordinate system starting at (0, 0) for A8
/// down to (7, 7) for H1.
#[repr(usize)]
#[derive(Clone, Copy, PartialEq, Eq, Debug)]
#[rustfmt::skip]
#[allow(missing_docs)]
pub enum Square {
    // LSB (0) = A8
    A8, B8, C8, D8, E8, F8, G8, H8,
    A7, B7, C7, D7, E7, F7, G7, H7,
    A6, B6, C6, D6, E6, F6, G6, H6,
    A5, B5, C5, D5, E5, F5, G5, H5,
    A4, B4, C4, D4, E4, F4, G4, H4,
    A3, B3, C3, D3, E3, F3, G3, H3,
    A2, B2, C2, D2, E2, F2, G2, H2,
    A1, B1, C1, D1, E1, F1, G1, H1,
    // MSB (63) = H1
}

use Square::*;

/// All the squares as an array.
#[rustfmt::skip]
static SQUARES: [Square; 64] = [
    A8, B8, C8, D8, E8, F8, G8, H8,
    A7, B7, C7, D7, E7, F7, G7, H7,
    A6, B6, C6, D6, E6, F6, G6, H6,
    A5, B5, C5, D5, E5, F5, G5, H5,
    A4, B4, C4, D4, E4, F4, G4, H4,
    A3, B3, C3, D3, E3, F3, G3, H3,
    A2, B2, C2, D2, E2, F2, G2, H2,
    A1, B1, C1, D1, E1, F1, G1, H1,
];

impl Square {
    /// Creates a new square from a rank and file. Returns None if
    /// the rank or file is invalid.
    pub fn new((rank, file): (i32, i32)) -> Option<Square> {
        match Self::valid_rf(rank, file) {
            true => Some(Square::from((rank, file))),
            false => None,
        }
    }

    /// Gets the rank (row) of the square within the board.
    /// Ranks are numbered from 0 (A8, B8, C8, ...) to
    /// 7 = (A1, B1, C1, ...)
    #[inline(always)]
    pub fn rank(self) -> i32 {
        i32::from(self) / 8
    }
    /// Gets the file (column) of the square within the board.
    /// Files are numbered from 0 (A8, A7, A6, ...) to
    /// 7 = (H8, H7, H6, ...)
    #[inline(always)]
    pub fn file(self) -> i32 {
        i32::from(self) % 8
    }

    /// Finds the difference between two squares.
    pub fn diff(self, other: Square) -> usize {
        usize::from(self).abs_diff(usize::from(other))
    }
    /// Finds the midpoint of two squares (should be same rank or file).
    pub fn mid(self, other: Square) -> Square {
        Square::from((usize::from(self) + usize::from(other)) / 2)
    }

    /// Gets the rank `self.rank()` and file `self.file()`
    pub fn rf(&self) -> (i32, i32) {
        (self.rank(), self.file())
    }

    /// Checks whether a (rank, file) coordinate pair is valid.
    ///     i.e. (0..8).contains(rank) && (0..8).contains(file)
    pub fn valid_rf(rank: i32, file: i32) -> bool {
        (0..8).contains(&rank) && (0..8).contains(&file)
    }
    /// Checks whether a position (0..64) is valid.
    pub fn valid_pos(pos: i32) -> bool {
        pos < 64
    }
    /// Gets the neighbour to this square in the direction `dir`. Returns
    /// `None` if there is no neighbour (off board).
    pub fn neighbour(&self, dir: Direction) -> Option<Square> {
        let (dr, df) = dir.vector();
        let rank = dr + self.rank();
        let file = df + self.file();
        match Self::valid_rf(rank, file) {
            true => Some(Square::from((rank, file))),
            false => None,
        }
    }
    /// Generates an iterator over squares starting from `self`, in a ray
    /// travelling in direction `dir`. Excludes the current square.
    pub fn ray(&self, dir: Direction) -> impl Iterator<Item = Self> {
        std::iter::successors(self.neighbour(dir), move |sq| sq.neighbour(dir))
    }

    /// Gets an iterator over every square from A1 to H8.
    pub fn iter() -> impl Iterator<Item = Square> {
        SQUARES.iter().copied()
    }

    /// Gets an iterator over the ray of squares between two squares,
    /// including the end, excluding the start. If no line connects the squares
    /// diagonally, returns None.
    pub fn to(self, end: Square) -> Option<impl Iterator<Item = Square>> {
        let dr = end.rank() - self.rank();
        let df = end.file() - self.file();

        let dir = match (dr, df) {
            (0, _) if df > 0 => Direction::East,
            (0, _) => Direction::West,
            (_, 0) if dr > 0 => Direction::South,
            (_, 0) => Direction::North,
            _ if dr == df && dr > 0 => Direction::SouthEast,
            _ if dr == df => Direction::NorthWest,
            _ if dr == -df && dr > 0 => Direction::SouthWest,
            _ if dr == -df => Direction::NorthEast,
            _ => return None,
        };

        let count = dr.abs().max(df.abs()) as usize;
        Some(self.ray(dir).take(count))
    }
}

impl Add<i32> for Square {
    type Output = Square;

    fn add(self, rhs: i32) -> Self::Output {
        let sq = i32::from(self) + rhs;
        debug_assert!((0..64).contains(&sq));
        Square::from(sq as usize)
    }
}
impl Sub<i32> for Square {
    type Output = Square;

    fn sub(self, rhs: i32) -> Self::Output {
        let sq = i32::from(self) - rhs;
        debug_assert!((0..64).contains(&sq));
        Square::from(sq as usize)
    }
}

impl TryFrom<&str> for Square {
    type Error = Error;

    fn try_from(val: &str) -> Result<Self> {
        let mut chars = val.chars();
        chars
            .next()
            .and_then(|file| chars.next().map(|rank| (rank, file)))
            .ok_or(Error::SquareFormat)
            .map(|(rank, file)| (rank as u8, file as u8))
            .and_then(Square::try_from)
    }
}
impl TryFrom<(u8, u8)> for Square {
    type Error = Error;

    fn try_from((rank, file): (u8, u8)) -> Result<Self> {
        let file = file.to_ascii_lowercase();

        match (rank, file) {
            (b'1'..=b'8', b'a'..=b'h') => {
                let rank = (7 - (rank - b'1')) as i32;
                let file = (file - b'a') as i32;
                Ok(Square::from((rank, file)))
            }
            _ => Err(Error::SquareFormat),
        }
    }
}
impl From<(i32, i32)> for Square {
    fn from((rank, file): (i32, i32)) -> Self {
        debug_assert!(Square::valid_rf(rank, file));
        Square::from((rank * 8 + file) as usize)
    }
}
impl From<usize> for Square {
    #[inline(always)]
    fn from(val: usize) -> Self {
        debug_assert!(val < 64);
        SQUARES[val]
    }
}
impl From<u16> for Square {
    fn from(val: u16) -> Self {
        Square::from(val as usize)
    }
}
impl From<Square> for usize {
    #[inline(always)]
    fn from(val: Square) -> Self {
        val as usize
    }
}
impl From<Square> for i32 {
    #[inline(always)]
    fn from(val: Square) -> Self {
        val as i32
    }
}
impl Display for Square {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "{}{}",
            char::from(97 + self.file() as u8),
            8 - self.rank()
        )
    }
}

/// Describes one of 8 possible directions (as compass points)
/// to go from a square.
#[derive(Clone, Copy, Debug)]
pub enum Direction {
    /// Up
    North,
    /// Up right
    NorthEast,
    /// Right
    East,
    /// Down right
    SouthEast,
    /// Down
    South,
    /// Down left
    SouthWest,
    /// Left
    West,
    /// Up left
    NorthWest,
}
impl Direction {
    /// Gets the direction vector for the `Direction`, as a
    /// (rank, file) tuple
    pub fn vector(&self) -> (i32, i32) {
        match self {
            Direction::North => (-1, 0),
            Direction::NorthEast => (-1, 1),
            Direction::East => (0, 1),
            Direction::SouthEast => (1, 1),
            Direction::South => (1, 0),
            Direction::SouthWest => (1, -1),
            Direction::West => (0, -1),
            Direction::NorthWest => (-1, -1),
        }
    }
    /// Returns an iterator over all the variants of `Direction`,
    /// in the order that they are declared.
    pub fn iter() -> impl Iterator<Item = Self> {
        use Direction::*;
        [
            North, NorthEast, East, SouthEast, South, SouthWest, West, NorthWest,
        ]
        .iter()
        .copied()
    }
    /// Returns an iterator over `North`, `East`, `South` and `West`
    /// in `Direction`.
    pub fn iter_rook() -> impl Iterator<Item = Self> {
        use Direction::*;
        [North, East, South, West].iter().copied()
    }
    /// Returns an iterator over `NorthEast`, `SouthEast`, `SouthWest`
    /// and `NorthWest` in `Direction`.
    pub fn iter_bishop() -> impl Iterator<Item = Self> {
        use Direction::*;
        [NorthEast, SouthEast, SouthWest, NorthWest].iter().copied()
    }
}
impl From<Direction> for usize {
    fn from(val: Direction) -> Self {
        val as usize
    }
}
