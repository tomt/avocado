//! Move representation & UndoInfo

use crate::{
    error::Error,
    game::{board::Board, castle::CastleSide, color::Color, piece::Piece, square::Square},
};
use std::fmt;

/// The four types of move.
#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum MoveType {
    /// A piece moves from one square to another, possibly
    /// capturing a piece.
    Standard = 0b0000,
    /// The king castles either kingside or queenside.
    Castle = 0b0100,
    /// Capturing a pawn en passant.
    EnPassant = 0b1000,
    /// Promoting a paw.
    Promotion = 0b1100,
}
impl fmt::Display for MoveType {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            MoveType::Standard => write!(f, "standard"),
            MoveType::Castle => write!(f, "castle"),
            MoveType::EnPassant => write!(f, "en passant"),
            MoveType::Promotion => write!(f, "promotion"),
        }
    }
}

/// The piece that a pawn is promoted to when it
/// reaches the back rank. Since a pawn cannot promote to
/// a pawn or a king, only the other 4 pieces are included.
#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum PromotionPiece {
    /// ♘
    Knight = 0b00,
    /// ♗
    Bishop = 0b01,
    ///♖
    Rook = 0b10,
    /// ♕
    Queen = 0b11,
}
impl PromotionPiece {
    /// Converts `self` to a `Piece`, taking the `color` as
    /// an argument.
    pub fn to_piece(self, color: Color) -> Piece {
        match self {
            PromotionPiece::Knight => Piece::knight(color),
            PromotionPiece::Bishop => Piece::bishop(color),
            PromotionPiece::Rook => Piece::rook(color),
            PromotionPiece::Queen => Piece::queen(color),
        }
    }
}
impl TryFrom<u8> for PromotionPiece {
    type Error = Error;

    fn try_from(value: u8) -> Result<Self, Self::Error> {
        match value {
            b'n' => Ok(PromotionPiece::Knight),
            b'b' => Ok(PromotionPiece::Bishop),
            b'r' => Ok(PromotionPiece::Rook),
            b'q' => Ok(PromotionPiece::Queen),
            _ => Err(Error::PromotionParse),
        }
    }
}
impl fmt::Display for PromotionPiece {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            PromotionPiece::Knight => write!(f, "n"),
            PromotionPiece::Bishop => write!(f, "b"),
            PromotionPiece::Rook => write!(f, "r"),
            PromotionPiece::Queen => write!(f, "q"),
        }
    }
}

/// Combines the start and end positions into a single u16
/// (stored in the first 12 bits).
fn encode_se(start: Square, end: Square) -> u16 {
    ((start as u16) << 10) | ((end as u16) << 4)
}

/// Stores the data needed to represent any move in a u16.
///
///`000000_000000_0000`
/// ^^^^^^ ------ ----   start `Square`
///        ^^^^^^ ----   end `Square`
///               ^^--  `MoveType`
///                 ^^  `PromotionPiece`
///                  ^  `CastleSide`
///
/// `PromotionPiece` and `CastleSide` overlap, however it is
/// not possible for both to be simultaneously present.
#[repr(transparent)]
#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub struct Move(u16);
impl From<u16> for Move {
    fn from(mv: u16) -> Self {
        Move(mv)
    }
}
impl From<Move> for u64 {
    fn from(Move(mv): Move) -> Self {
        mv as u64
    }
}
impl Move {
    /// A null move.
    pub fn null() -> Self {
        Move(0)
    }
    /// Converts the move to an option, None if the move is null.
    pub fn not_null(self) -> Option<Self> {
        match self {
            Move(0) => None,
            _ => Some(self),
        }
    }

    /// Checks whether the move is a capture.
    pub fn is_capture(&self, board: &Board) -> bool {
        board[self.start()].is_some()
    }

    /// A standard move, where a piece goes from `start` to `end`
    pub fn standard(start: Square, end: Square) -> Self {
        Self(encode_se(start, end) | MoveType::Standard as u16)
    }
    /// A castle, storing the king's `start` and `end` positions
    pub fn castle(start: Square, end: Square, side: CastleSide) -> Self {
        Self(encode_se(start, end) | MoveType::Castle as u16 | side as u16)
    }
    /// En Passant, storing the pawn's `start` position and `end` position
    /// (where the pawn ends up, not the position of the captured pawn).
    pub fn en_passant(start: Square, end: Square) -> Self {
        Self(encode_se(start, end) | MoveType::EnPassant as u16)
    }
    /// Promotion, storing the pawn's `start` position and `end` position,
    /// along with the `piece` it is promoted to.
    pub fn promotion(start: Square, end: Square, piece: PromotionPiece) -> Self {
        Self(encode_se(start, end) | MoveType::Promotion as u16 | piece as u16)
    }

    /// Gets the `MoveType`.
    pub fn move_type(&self) -> MoveType {
        match self.0 & 0b1100 {
            0b0000 => MoveType::Standard,
            0b0100 => MoveType::Castle,
            0b1000 => MoveType::EnPassant,
            0b1100 => MoveType::Promotion,
            _ => unreachable!(),
        }
    }
    /// Gets the `PromotionPiece`
    pub fn promotion_piece(&self) -> PromotionPiece {
        match self.0 & 0b11 {
            0b00 => PromotionPiece::Knight,
            0b01 => PromotionPiece::Bishop,
            0b10 => PromotionPiece::Rook,
            0b11 => PromotionPiece::Queen,
            _ => unreachable!(),
        }
    }
    /// Gets the `CastleSide`
    pub fn castle_side(&self) -> CastleSide {
        match self.0 & 0b1 {
            0b0 => CastleSide::QueenSide,
            0b1 => CastleSide::KingSide,
            _ => unreachable!(),
        }
    }

    /// Gets the start `Square`
    pub fn start(&self) -> Square {
        Square::from(self.0 >> 10)
    }
    /// Gets the end `Square`
    pub fn end(&self) -> Square {
        Square::from((self.0 >> 4) & 0b111111)
    }
}
impl fmt::Display for Move {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        // check for null move
        if self.0 == 0 {
            return write!(f, "0000");
        }

        let start = self.start();
        let end = self.end();

        match self.move_type() {
            MoveType::Promotion => write!(f, "{}{}{}", start, end, self.promotion_piece()),
            _ => write!(f, "{}{}", start, end),
        }
    }
}
