//! Move generation.

use crate::game::{
    bitboard::BitBoard,
    board::Board,
    castle::CastleSide,
    color::Color,
    magics::Magics,
    move_list::MoveList,
    moves::Move,
    piece::Piece,
    square::Square::{self, *},
};
use std::ops::BitOr;

/// Contains associated constants & methods to generate seperate
/// move generators for black & white.
pub trait Player {
    /// The [`Color`] for this player.
    const COLOR: Color;
    /// The [`Color`] for the enemy player.
    const ENEMY: Color = Self::COLOR.enemy();

    /// Bitboard containing the row of pawns that have been
    /// pushed by a single square.
    const BB_PUSHED_PAWN_RANK: BitBoard;
    /// BitBoard of the back rank (castling).
    const BB_BACK_RANK: BitBoard;
    /// BitBoard of the back rank on the other side (promotion).
    const BB_BACK_RANK_OPP: BitBoard = Self::BB_BACK_RANK.reverse();

    /// Squares that must not be attacked for ks castling.
    const CASTLE_SQUARES_KS: (Square, Square);
    /// Squares that must not be attacked for qs castling.
    const CASTLE_SQUARES_QS: (Square, Square);
    /// Start/end squares for ks castling.
    const CASTLE_SE_KS: (Square, Square);
    /// Start/end squares for qs castling.
    const CASTLE_SE_QS: (Square, Square);

    /// Offset for pushing a pawn forwards.
    const UP_OFFSET: i32;
    /// Offset for capturing on the left with a pawn.
    const UP_LEFT_OFFSET: i32;
    /// Offset for capturing on the right with a pawn.
    const UP_RIGHT_OFFSET: i32;

    /// Shifts a bitboard up the board.
    fn shift_up(bb: BitBoard) -> BitBoard;
    /// Shifts a bitboard down the board.
    fn shift_down(bb: BitBoard) -> BitBoard;
    /// Shifts a bitboard up and left (left pawn cap).
    fn shift_up_left(bb: BitBoard) -> BitBoard;
    /// Shifts a bitboard up and right (right pawn cap).
    fn shift_up_right(bb: BitBoard) -> BitBoard;
}

/// Unit type for the White player, implements [`Player`].
pub struct White;
impl Player for White {
    const COLOR: Color = Color::White;

    const BB_PUSHED_PAWN_RANK: BitBoard = BitBoard(0x0000_ff00_0000_0000);
    const BB_BACK_RANK: BitBoard = BitBoard(0xff00_0000_0000_0000);

    const CASTLE_SQUARES_KS: (Square, Square) = (F1, G1);
    const CASTLE_SQUARES_QS: (Square, Square) = (C1, D1);
    const CASTLE_SE_KS: (Square, Square) = (E1, G1);
    const CASTLE_SE_QS: (Square, Square) = (E1, C1);

    const UP_OFFSET: i32 = -8;
    const UP_LEFT_OFFSET: i32 = -9;
    const UP_RIGHT_OFFSET: i32 = -7;

    #[inline(always)]
    fn shift_up(bb: BitBoard) -> BitBoard {
        bb >> 8
    }
    #[inline(always)]
    fn shift_down(bb: BitBoard) -> BitBoard {
        bb << 8
    }
    #[inline(always)]
    fn shift_up_left(bb: BitBoard) -> BitBoard {
        (bb & BitBoard(0xfefefefefefefefe)) >> 9
    }
    #[inline(always)]
    fn shift_up_right(bb: BitBoard) -> BitBoard {
        (bb & BitBoard(0x7f7f7f7f7f7f7f7f)) >> 7
    }
}

/// Unit type for the Black player, implements [`Player`].
pub struct Black;
impl Player for Black {
    const COLOR: Color = Color::Black;

    const BB_PUSHED_PAWN_RANK: BitBoard = BitBoard(0x0000_0000_00ff_0000);
    const BB_BACK_RANK: BitBoard = BitBoard(0x0000_0000_0000_00ff);

    const CASTLE_SQUARES_KS: (Square, Square) = (F8, G8);
    const CASTLE_SQUARES_QS: (Square, Square) = (C8, D8);
    const CASTLE_SE_KS: (Square, Square) = (E8, G8);
    const CASTLE_SE_QS: (Square, Square) = (E8, C8);

    const UP_OFFSET: i32 = 8;
    const UP_LEFT_OFFSET: i32 = 7;
    const UP_RIGHT_OFFSET: i32 = 9;

    #[inline(always)]
    fn shift_up(bb: BitBoard) -> BitBoard {
        bb << 8
    }
    #[inline(always)]
    fn shift_down(bb: BitBoard) -> BitBoard {
        bb >> 8
    }
    #[inline(always)]
    fn shift_up_left(bb: BitBoard) -> BitBoard {
        (bb & BitBoard(0xfefefefefefefefe)) << 7
    }
    #[inline(always)]
    fn shift_up_right(bb: BitBoard) -> BitBoard {
        (bb & BitBoard(0x7f7f7f7f7f7f7f7f)) << 9
    }
}

/// Legal move generator
#[derive(Debug, Default)]
pub struct MoveGen {
    magics: Magics,
}

impl MoveGen {
    fn add_moves<F>(&self, move_list: &mut MoveList, moves: BitBoard, build_move: F)
    where
        F: Fn(Square) -> Move,
    {
        for end in moves {
            move_list.push(build_move(end));
        }
    }

    fn attacking_sq<P: Player>(&self, board: &Board, sq: Square) -> BitBoard {
        let occupancy = board.bb_occupied();

        let queens = board.bb(Piece::queen(P::ENEMY));
        let bishops_or_queens = queens | board.bb(Piece::bishop(P::ENEMY));
        let rooks_or_queens = queens | board.bb(Piece::rook(P::ENEMY));

        (self.magics.bishop(sq, occupancy) & bishops_or_queens)
            | (self.magics.rook(sq, occupancy) & rooks_or_queens)
            | (self.magics.knight(sq) & board.bb(Piece::knight(P::ENEMY)))
            | (self.magics.king(sq) & board.bb(Piece::king(P::ENEMY)))
            | (self.magics.pawn_attacks(sq, P::COLOR) & board.bb(Piece::pawn(P::ENEMY)))
    }
    fn is_square_attacked<P: Player>(
        &self,
        board: &Board,
        sq: Square,
        occupancy: BitBoard,
    ) -> bool {
        // checks whether the square is attacked by a knight, king or pawn
        if !(self.magics.knight(sq) & board.bb(Piece::knight(P::ENEMY))).is_empty()
            || !(self.magics.king(sq) & board.bb(Piece::king(P::ENEMY))).is_empty()
            || !(self.magics.pawn_attacks(sq, P::COLOR) & board.bb(Piece::pawn(P::ENEMY)))
                .is_empty()
        {
            return true;
        }

        let queens = board.bb(Piece::queen(P::ENEMY));

        ({
            let bishops_or_queens = queens | board.bb(Piece::bishop(P::ENEMY));
            !(self.magics.bishop(sq, occupancy) & bishops_or_queens).is_empty()
        } || {
            let rooks_or_queens = queens | board.bb(Piece::rook(P::ENEMY));
            !(self.magics.rook(sq, occupancy) & rooks_or_queens).is_empty()
        })
    }

    fn add_king_moves<P: Player, const CAPS: bool>(&self, move_list: &mut MoveList, board: &Board) {
        let king_bb = board.bb(Piece::king(P::COLOR));
        let king_pos = king_bb.lsb_idx();
        let occupancy = board.bb_occupied() & !king_bb;
        let moves = match CAPS {
            true => self.magics.king(king_pos) & board.bb_all(P::ENEMY),
            false => self.magics.king(king_pos) & !board.bb_all(P::COLOR),
        };

        moves
            .into_iter()
            .filter(|&end| !self.is_square_attacked::<P>(board, end, occupancy))
            .for_each(|end| move_list.push(Move::standard(king_pos, end)));
    }
    fn add_castling_moves<P: Player>(&self, move_list: &mut MoveList, board: &Board) {
        let board_occ = board.bb_occupied();

        let mut add_castle_move = |side, mask, (start, end), (sq0, sq1)| {
            let occupancy: BitBoard = mask & P::BB_BACK_RANK & board_occ;

            if board.rights().can_castle(P::COLOR, side)
                && occupancy.is_empty()
                && !self.is_square_attacked::<P>(board, sq0, board_occ)
                && !self.is_square_attacked::<P>(board, sq1, board_occ)
            {
                move_list.push(Move::castle(start, end, side))
            }
        };

        add_castle_move(
            CastleSide::KingSide,
            BitBoard(0x6000000000000060),
            P::CASTLE_SE_KS,
            P::CASTLE_SQUARES_KS,
        );
        add_castle_move(
            CastleSide::QueenSide,
            BitBoard(0xe0000000000000e),
            P::CASTLE_SE_QS,
            P::CASTLE_SQUARES_QS,
        );
    }
    fn add_pawn_pushes<P: Player>(
        &self,
        move_list: &mut MoveList,
        board: &Board,
        pawns: BitBoard,
        block: BitBoard,
    ) {
        let offset = P::UP_OFFSET;
        let empty = !board.bb_occupied();
        let back_rank = P::BB_BACK_RANK_OPP;

        // add forward pushes
        let mut forward_pushes = P::shift_up(pawns) & empty;
        let promotions = forward_pushes & back_rank & block;
        forward_pushes &= !promotions;
        self.add_moves(move_list, forward_pushes & block, |end| {
            Move::standard(end - offset, end)
        });

        // add promotion pushes
        promotions.into_iter().for_each(|end| {
            move_list.push_promotion(end - offset, end);
        });

        // add double pushes
        let possible_double_pushes = forward_pushes & P::BB_PUSHED_PAWN_RANK;
        let double_pushes = P::shift_up(possible_double_pushes) & empty & block;
        let double_offset = offset * 2;
        self.add_moves(move_list, double_pushes, |end| {
            Move::standard(end - double_offset, end)
        });
    }
    #[allow(clippy::too_many_arguments)]
    fn add_pawn_captures<F, P: Player>(
        &self,
        move_list: &mut MoveList,
        board: &Board,
        pawns: BitBoard,
        capture: BitBoard,
        block: BitBoard,
        offset: i32,
        shift_bb: F,
    ) where
        F: Fn(BitBoard) -> BitBoard,
    {
        let enemy = board.bb_all(P::ENEMY);
        let back_rank = P::BB_BACK_RANK_OPP;

        // add captures
        let mut caps = shift_bb(pawns) & enemy & capture;
        let promotion_caps = caps & back_rank;
        caps &= !promotion_caps;
        self.add_moves(move_list, caps, |end| Move::standard(end - offset, end));

        // add promotion captures
        for end in promotion_caps {
            move_list.push_promotion(end - offset, end);
        }

        // add en passant captures
        if let Some(en_passant_sq) = board.en_passant() {
            // get a bitboard containing en_passant_sq
            let en_passant_bb = BitBoard::from(en_passant_sq);
            // bitboard either containg en_passant_sq or no squares. if caps != 0
            // then an en passant capture can occur.
            // AND this bitboard with the capture mask OR the capture mask, shifted
            // "up" by 1 square, which will contain the en passant square if the pawn
            // which may be captured is in the capture mask.
            // AND this with the block mask, to check whether the final position
            // blocks a check, if there is one.
            let caps = shift_bb(pawns) & en_passant_bb & (capture | P::shift_up(capture) | block);

            // if caps is not empty, then the pawn may be able to capture en passant.
            // however, this may result in a discovered check if there is a rook or queen
            // on one side and the king on the other side, with no other piece in between.
            // to resolve this, the new occupancy is computed, and rook / queen moves are
            // checked to see whether they would cause a check.
            if !caps.is_empty() {
                // square that the friendly pawn started on
                let start_sq = en_passant_sq - offset;

                // swap the bits for the en passant square, and both pawns
                // to find the new occupancy
                let occupancy = board.bb_occupied()
                    ^ (P::shift_down(en_passant_bb) | en_passant_bb | BitBoard::from(start_sq));
                // find any rooks or queens that are now attacking the king
                let king_pos = board.bb(Piece::king(P::COLOR)).lsb_idx();
                let rooks_or_queens =
                    board.bb(Piece::queen(P::ENEMY)) | board.bb(Piece::rook(P::ENEMY));

                let attackers = self.magics.rook(king_pos, occupancy) & rooks_or_queens;

                // doesn't result in check if attackers is empty
                if attackers.is_empty() {
                    let end_sq = en_passant_sq - P::UP_OFFSET;
                    move_list.push(Move::en_passant(start_sq, end_sq));
                }
            }
        }
    }
    fn add_pawn_moves<P: Player, const CAPS: bool>(
        &self,
        move_list: &mut MoveList,
        board: &Board,
        pinned: BitBoard,
        block: BitBoard,
        capture: BitBoard,
    ) {
        let pawns = board.bb(Piece::pawn(P::COLOR)) & !pinned;

        if !CAPS {
            self.add_pawn_pushes::<P>(move_list, board, pawns, block);
        }
        self.add_pawn_captures::<_, P>(
            move_list,
            board,
            pawns,
            capture,
            block,
            P::UP_RIGHT_OFFSET,
            P::shift_up_right,
        );
        self.add_pawn_captures::<_, P>(
            move_list,
            board,
            pawns,
            capture,
            block,
            P::UP_LEFT_OFFSET,
            P::shift_up_left,
        );
    }
    fn add_pinned_moves<P: Player, const CAPS: bool>(
        &self,
        move_list: &mut MoveList,
        board: &Board,
        block: BitBoard,
        capture: BitBoard,
    ) -> BitBoard {
        let friendly = board.bb_all(P::COLOR);
        let enemy = board.bb_all(P::ENEMY);
        let occupancy = board.bb_occupied();

        let king_pos = board.bb(Piece::king(P::COLOR)).lsb_idx();

        let q = board.bb(Piece::queen(P::ENEMY));
        let rq = q | board.bb(Piece::rook(P::ENEMY));
        let bq = q | board.bb(Piece::bishop(P::ENEMY));

        let bq_attackers = self.magics.bishop(king_pos, enemy) & bq;
        let rq_attackers = self.magics.rook(king_pos, enemy) & rq;

        let mut get_pins = |attackers: BitBoard, is_rook| {
            attackers
                .into_iter()
                .filter_map(|attacker_pos| {
                    let ray = self.magics.ray(king_pos, attacker_pos);
                    let pinned_pieces = friendly & ray;

                    // a piece is only pinned if there if there is only one
                    // piece in the ray.
                    if pinned_pieces.is_power_of_two() {
                        let pinned_pos = pinned_pieces.lsb_idx();
                        let move_mask = ray & !friendly & (capture | block);

                        match board[pinned_pos] {
                            // a rook can only move along an absolute pin if the attacker
                            // is attacking in a straight line
                            Some(p) if (p.is_queen() || p.is_rook()) && is_rook => {
                                let moves = self.magics.rook(pinned_pos, occupancy)
                                    & move_mask
                                    & match CAPS {
                                        true => enemy,
                                        false => BitBoard::FULL,
                                    };

                                self.add_moves(move_list, moves, |end| {
                                    Move::standard(pinned_pos, end)
                                })
                            }
                            // a bishop can only move along an absolute pin if the attacker
                            // is attacking diagonally
                            Some(p) if (p.is_queen() || p.is_bishop()) && !is_rook => {
                                let moves = self.magics.bishop(pinned_pos, occupancy)
                                    & move_mask
                                    & match CAPS {
                                        true => enemy,
                                        false => BitBoard::FULL,
                                    };
                                self.add_moves(move_list, moves, |end| {
                                    Move::standard(pinned_pos, end)
                                })
                            }
                            // a pinned pawn can capture (possibly en passant) if a bishop is pinning
                            // it, or push forward if a rook is pinning it.
                            Some(p) if p.is_pawn() => {
                                let pawns = BitBoard::from(pinned_pos);
                                let block_mask = block & ray;

                                match is_rook {
                                    true => {
                                        if !CAPS {
                                            self.add_pawn_pushes::<P>(
                                                move_list, board, pawns, block_mask,
                                            )
                                        }
                                    }
                                    false => {
                                        let capture_mask = capture & ray;

                                        self.add_pawn_captures::<_, P>(
                                            move_list,
                                            board,
                                            pawns,
                                            capture_mask,
                                            block_mask,
                                            P::UP_RIGHT_OFFSET,
                                            P::shift_up_right,
                                        );
                                        self.add_pawn_captures::<_, P>(
                                            move_list,
                                            board,
                                            pawns,
                                            capture_mask,
                                            block_mask,
                                            P::UP_LEFT_OFFSET,
                                            P::shift_up_left,
                                        );
                                    }
                                }
                            }
                            // pinned knight can't move, king cannot be pinned.
                            _ => (),
                        };

                        Some(pinned_pieces)
                    } else {
                        None
                    }
                })
                .reduce(BitOr::bitor)
                .unwrap_or_default()
        };

        get_pins(bq_attackers, false) | get_pins(rq_attackers, true)
    }
    #[inline(always)]
    fn add_nbrq_moves<F, G, P: Player, const CAPS: bool>(
        &self,
        move_list: &mut MoveList,
        board: &Board,
        pinned: BitBoard,
        move_mask: BitBoard,
        create_piece: F,
        gen_moves: G,
    ) where
        F: Fn(Color) -> Piece,
        G: Fn(Square, BitBoard) -> BitBoard,
    {
        let occupancy = board.bb_occupied();
        let enemy = board.bb_all(P::ENEMY);
        let not_friendly = !board.bb_all(P::COLOR);
        let nbrq = board.bb(create_piece(P::COLOR)) & !pinned;

        for start in nbrq {
            let moves = match CAPS {
                true => gen_moves(start, occupancy) & move_mask & enemy,
                false => gen_moves(start, occupancy) & move_mask & not_friendly,
            };

            self.add_moves(move_list, moves, |end| Move::standard(start, end))
        }
    }

    fn add_all_moves<P: Player, const CAPS: bool>(&self, move_list: &mut MoveList, board: &Board) {
        // generate king moves first
        self.add_king_moves::<P, CAPS>(move_list, board);

        let king_pos = board.bb(Piece::king(P::COLOR)).lsb_idx();
        let attackers = self.attacking_sq::<P>(board, king_pos);

        let (block, capture) = match attackers.count_ones() {
            // if there is a double check, the only response is to move the king
            2 => return,
            // if there is a single check, moves must either block the check, move
            // out of check, or capture the piece giving check. Absolute pins must
            // be considered. Castling is not allowed.
            1 => {
                let attacker_pos = attackers.lsb_idx();

                // bitboard containing set of squares which a piece can move to
                let block = match board[attacker_pos] {
                    Some(p) if p.is_queen() || p.is_bishop() || p.is_rook() => {
                        self.magics.ray(king_pos, attacker_pos)
                    }
                    _ => BitBoard::EMPTY,
                };

                (block, attackers)
            }
            // if there are no checks, only the limitations of absolute pins need
            // to be considered.
            0 => {
                if !CAPS {
                    self.add_castling_moves::<P>(move_list, board);
                }

                (BitBoard::FULL, BitBoard::FULL)
            }
            attackers => panic!("There are {} pieces attacking the king.", attackers),
        };

        let pinned = self.add_pinned_moves::<P, CAPS>(move_list, board, block, capture);

        self.add_pawn_moves::<P, CAPS>(move_list, board, pinned, block, capture);

        // queen, rook, bishop, knight
        let move_mask = block | capture;
        self.add_nbrq_moves::<_, _, P, CAPS>(
            move_list,
            board,
            pinned,
            move_mask,
            Piece::knight,
            |sq, _| self.magics.knight(sq),
        );
        self.add_nbrq_moves::<_, _, P, CAPS>(
            move_list,
            board,
            pinned,
            move_mask,
            Piece::bishop,
            |sq, occupancy| self.magics.bishop(sq, occupancy),
        );
        self.add_nbrq_moves::<_, _, P, CAPS>(
            move_list,
            board,
            pinned,
            move_mask,
            Piece::rook,
            |sq, occupancy| self.magics.rook(sq, occupancy),
        );
        self.add_nbrq_moves::<_, _, P, CAPS>(
            move_list,
            board,
            pinned,
            move_mask,
            Piece::queen,
            |sq, occupancy| self.magics.queen(sq, occupancy),
        );
    }

    /// Checks whether the active player is in check
    pub fn is_in_check(&self, board: &Board) -> bool {
        let king_pos = board.king_pos();
        let occupancy = board.bb_occupied();

        match board.to_play() {
            Color::White => self.is_square_attacked::<White>(board, king_pos, occupancy),
            Color::Black => self.is_square_attacked::<Black>(board, king_pos, occupancy),
        }
    }

    /// Gets a bitboard of all pieces checking the friendly king.
    pub fn checkers(&self, board: &Board) -> BitBoard {
        let king_pos = board.king_pos();
        match board.to_play() {
            Color::White => self.attacking_sq::<White>(board, king_pos),
            Color::Black => self.attacking_sq::<Black>(board, king_pos),
        }
    }

    /// Adds the legal moves for the `board` position to `move_list`.
    pub fn gen_moves(&self, move_list: &mut MoveList, board: &Board) {
        move_list.clear();
        match board.to_play() {
            Color::White => self.add_all_moves::<White, false>(move_list, board),
            Color::Black => self.add_all_moves::<Black, false>(move_list, board),
        }
    }

    /// Adds only the legal captures to the move list.
    pub fn gen_captures(&self, move_list: &mut MoveList, board: &Board) {
        move_list.clear();
        match board.to_play() {
            Color::White => self.add_all_moves::<White, true>(move_list, board),
            Color::Black => self.add_all_moves::<Black, true>(move_list, board),
        }
    }
}
