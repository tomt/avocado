//! A move list on the stack.

use crate::game::{
    moves::{Move, PromotionPiece},
    square::Square,
};
use std::{fmt, ops::Index};

const CAPACITY: usize = 256;

/// Move list (stack allocated).
pub struct MoveList {
    len: usize,
    moves: [Move; CAPACITY],
}

impl fmt::Debug for MoveList {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        f.debug_struct("MoveList")
            .field("len", &self.len)
            .field("moves", &&self.moves[0..self.len])
            .finish()
    }
}
impl<'a> IntoIterator for &'a MoveList {
    type Item = Move;
    type IntoIter = std::iter::Copied<std::slice::Iter<'a, Move>>;

    fn into_iter(self) -> Self::IntoIter {
        self.moves[0..self.len].iter().copied()
    }
}
impl Index<usize> for MoveList {
    type Output = Move;

    fn index(&self, index: usize) -> &Self::Output {
        debug_assert!(index < self.len);
        self.moves.index(index)
    }
}
impl Default for MoveList {
    fn default() -> Self {
        Self {
            len: 0,
            moves: [Move::null(); CAPACITY],
        }
    }
}
impl MoveList {
    /// Adds move to list.
    #[inline(always)]
    pub fn push(&mut self, mv: Move) {
        self.moves[self.len] = mv;
        self.len += 1;
    }
    /// Adds a promotion to the list.
    #[inline(always)]
    pub fn push_promotion(&mut self, start: Square, end: Square) {
        self.push(Move::promotion(start, end, PromotionPiece::Knight));
        self.push(Move::promotion(start, end, PromotionPiece::Bishop));
        self.push(Move::promotion(start, end, PromotionPiece::Rook));
        self.push(Move::promotion(start, end, PromotionPiece::Queen));
    }
    /// Removes move from end.
    pub fn pop(&mut self) -> Option<Move> {
        self.len = self.len.saturating_sub(1);
        match self.len {
            0 => None,
            len => Some(self.moves[len]),
        }
    }
    /// Clears the list.
    pub fn clear(&mut self) {
        self.len = 0;
    }
    /// Length of movelist.
    pub fn len(&self) -> usize {
        self.len
    }
    /// Is the list empty?
    pub fn is_empty(&self) -> bool {
        self.len == 0
    }
    /// Iterate over the list.
    pub fn iter(&self) -> impl Iterator<Item = Move> + '_ {
        self.moves[0..self.len].iter().copied()
    }
    /// Sort the moves by key.
    pub fn sort_unstable_by_key<K, F>(&mut self, f: F)
    where
        F: FnMut(&Move) -> K,
        K: Ord,
    {
        self.moves[0..self.len].sort_unstable_by_key(f)
    }
}
