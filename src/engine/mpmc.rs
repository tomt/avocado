//! Multi-producer, multi-consumer queue.

use std::sync::{mpsc, Arc, Mutex};

/// Creates a multi-producer, multi-consumer channel.
pub fn channel<T>() -> (mpsc::Sender<T>, Receiver<T>) {
    let (sender, receiver) = mpsc::channel();
    let receiver = Arc::new(Mutex::new(receiver));

    (sender, Receiver(receiver))
}

/// Receiver half of a multi-consumer queue.
#[derive(Debug, Clone)]
pub struct Receiver<T>(Arc<Mutex<mpsc::Receiver<T>>>);

impl<T> Receiver<T> {
    /// Iterator that blocks waiting for messages.
    pub fn iter(&self) -> Iter<'_, T> {
        Iter { receiver: self }
    }

    /// Blocks, waits for a message.
    pub fn recv(&self) -> Result<T, mpsc::RecvError> {
        match self.0.lock() {
            Ok(m) => m.recv(),
            Err(_) => Err(mpsc::RecvError),
        }
    }

    /// Attempts to wait for a value, blocks.
    pub fn try_recv(&self) -> Result<T, mpsc::TryRecvError> {
        match self.0.try_lock() {
            Ok(m) => m.try_recv(),
            Err(_) => Err(mpsc::TryRecvError::Empty),
        }
    }
}

/// Iterator over mpmc receiver.
pub struct Iter<'a, T> {
    receiver: &'a Receiver<T>,
}
impl<'a, T> Iterator for Iter<'a, T> {
    type Item = T;

    fn next(&mut self) -> Option<T> {
        self.receiver.recv().ok()
    }
}
