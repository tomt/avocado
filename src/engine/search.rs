//! Implementation of iterative deepening search.

use crate::uci::response::{Info, Score, UciResponse};
use crate::{
    engine::{
        eval,
        evaluation::{DRAW, NEG_INF, POS_INF},
        ordering,
        pv::Pv,
        transpositions::{NodeType, Transpositions},
    },
    game::{
        board::{Board, UndoInfo},
        move_gen::MoveGen,
        move_list::MoveList,
        moves::Move,
    },
};
use std::{
    sync::atomic::{AtomicBool, Ordering},
    time::Instant,
};

/// Maximum search depth.
const MAX_DEPTH: usize = 64;
/// Depth reduction for null moves.
const NULL_REDUCTION: usize = 4;

/// Performs an iterative deepening negamax search.
pub struct Search<'a, F> {
    gen: &'a MoveGen,
    stop: &'a AtomicBool,
    board: Board,
    transpositions: Transpositions,
    history: [[i32; 64]; 12],
    killers: [[Move; 2]; MAX_DEPTH],
    curr_depth: usize,
    nodes: usize,
    on_msg: F,
}

impl<'a, F> Search<'a, F>
where
    F: FnMut(UciResponse),
{
    /// Sends a message.
    pub fn send(&mut self, msg: UciResponse) {
        (self.on_msg)(msg)
    }

    /// Creates a new `Search` structure.
    pub fn new(board: Board, gen: &'a MoveGen, stop: &'a AtomicBool, on_msg: F) -> Self {
        Search {
            gen,
            stop,
            board,
            transpositions: Transpositions::default(),
            history: [[0; 64]; 12],
            killers: [[Move::null(); 2]; MAX_DEPTH],
            curr_depth: 0,
            nodes: 0,
            on_msg,
        }
    }

    /// Performs an iterative deepening search.
    pub fn iterative_deepening(&mut self, depth: usize, time: usize) -> Option<(Move, i32)> {
        let start = Instant::now();
        let mut best_move = None;

        for depth in 1..=depth.clamp(1, MAX_DEPTH) {
            let mut pv = Pv::default();
            let start_iter = Instant::now();

            // needed to calculate ply from root.
            self.curr_depth = depth;
            self.nodes = 0;

            let score = self.negamax(depth, NEG_INF, POS_INF, true, &mut pv);

            let end = Instant::now();
            let millis = (end - start).as_millis() as usize;
            let millis_iter = (end - start_iter).as_millis() as usize;

            if let Some(mv) = pv.first() {
                best_move = Some((mv, score));
            }

            self.send(UciResponse::Info(Box::new(Info {
                depth: Some(depth),
                time: Some(millis as u16),
                pv: Some(pv),
                nodes: Some(self.nodes as u32),
                score: Some((Score::Cp(score), None)),
                nps: Some((1000 * self.nodes / (1 + millis_iter)) as u32),
                hashfull: Some(self.transpositions.hashfull() as u32),
                ..Default::default()
            })));

            // stop searching if time has run out.
            if millis > time || self.stop.load(Ordering::Relaxed) {
                break;
            }
        }

        if let Some((best_move, _)) = best_move {
            self.send(UciResponse::BestMove(best_move, None));
        }

        best_move
    }

    /// A simplified iterative deepening function, no time control
    /// or stats calculated.
    pub fn iterative_deepening_basic(&mut self, depth: usize) -> Option<(Move, i32)> {
        let mut best = None;

        for depth in 1..=depth.clamp(1, MAX_DEPTH) {
            self.curr_depth = depth;

            let mut pv = Pv::default();
            let score = self.negamax(depth, NEG_INF, POS_INF, true, &mut pv);
            if let Some(mv) = pv.first() {
                best = Some((mv, score));
            }
        }

        best
    }

    /// Negamax search with:
    /// * alpha-beta pruning: https://web.archive.org/web/20071030084528/http://www.brucemo.com/compchess/programming/alphabeta.htm
    /// * pv search: https://web.archive.org/web/20071030220825/http://www.brucemo.com/compchess/programming/pvs.htm
    fn negamax(&mut self, depth: usize, mut alpha: i32, beta: i32, null: bool, pv: &mut Pv) -> i32 {
        self.nodes += 1;

        let (tt_move, tt_score) = self.tt_lookup(depth, alpha, beta);

        if let Some(score) = tt_score {
            return score;
        }
        if depth == 0 {
            pv.clear();
            let score = self.quiescent(alpha, beta);
            self.tt_store(depth, score, NodeType::Exact, None);
            return score;
        }

        let mut moves = MoveList::default();
        self.gen.gen_moves(&mut moves, &self.board);

        if moves.is_empty() {
            return match self.gen.is_in_check(&self.board) {
                true => NEG_INF - self.ply(depth) as i32,
                false => DRAW,
            };
        } else if self.board.is_draw() {
            return DRAW;
        }

        // null move pruning
        if null && self.null_move(depth, alpha, beta) >= beta {
            return beta;
        }

        // move ordering (PV, Transpos, Killer, History, MVV/LVA).
        ordering::sort(
            &mut moves,
            &self.board,
            tt_move,
            self.killers[self.ply(depth)],
            &self.history,
        );

        let mut info = UndoInfo::default();
        let mut is_pv = false;
        let mut node_type = NodeType::Alpha;
        let mut best_move = None;
        let mut local_pv = Pv::default();

        for mv in &moves {
            self.board.make_move(mv, &mut info);
            let score = self.pv_search(depth, alpha, beta, is_pv, &mut local_pv);
            self.board.undo_move(mv, &info);

            if score >= beta {
                self.tt_store(depth, beta, NodeType::Beta, Some(mv));
                self.update_killer(mv);
                return beta;
            } else if score > alpha {
                alpha = score;
                is_pv = true;
                node_type = NodeType::Exact;
                best_move = Some(mv);
                pv.update(mv, &local_pv);
                self.update_history(mv, depth);
            }
        }

        self.tt_store(depth, alpha, node_type, best_move);

        alpha
    }

    /// Gets the ply from the root for the current depth.
    fn ply(&self, depth: usize) -> usize {
        self.curr_depth - depth
    }

    /// Looks up the current position in the transposition table.
    fn tt_lookup(&self, depth: usize, alpha: i32, beta: i32) -> (Option<Move>, Option<i32>) {
        self.transpositions
            .lookup(self.board.zobrist(), depth, alpha, beta)
    }
    /// Stores a value in the transposition table.
    fn tt_store(&mut self, depth: usize, score: i32, node_type: NodeType, best_move: Option<Move>) {
        self.transpositions
            .store(self.board.zobrist(), depth, node_type, best_move, score);
    }

    /// Updates the history table entry for a move that caused alpha to increase.
    fn update_history(&mut self, mv: Move, depth: usize) {
        // captures are ordered by MVV/LVA.
        if !mv.is_capture(&self.board) {
            let piece = usize::from(self.board[mv.start()].unwrap());
            let end = usize::from(mv.end());
            self.history[piece][end] += (depth * depth) as i32;
        }
    }
    /// Updates the killer move entries for the current depth. Called on a beta
    /// cutoff.
    fn update_killer(&mut self, mv: Move) {
        // captures are ordered by MVV/LVA.
        if !mv.is_capture(&self.board) {
            let idx = self.board.half_move_count();

            // Set previous killer 0 -> 1, set killer 0 to provided move.
            self.killers[idx][1] = self.killers[idx][0];
            self.killers[idx][0] = mv;
        }
    }

    /// Performs a search with a reduced window aimed at proving that a move is
    /// worse than the assumed pv move, which is faster. Falls back to a standard
    /// search.
    fn pv_search(&mut self, depth: usize, alpha: i32, beta: i32, is_pv: bool, pv: &mut Pv) -> i32 {
        if is_pv {
            // found a pv move (assumed), just check that other moves are worse.
            let score = -self.negamax(depth - 1, -alpha - 1, -alpha, true, pv);
            // if score <= alpha or score >= beta then it is valid.
            if !(alpha + 1..beta).contains(&score) {
                return score;
            }
        }

        // If no pv move found, or search did not give value in correct range,
        // do a full search.
        -self.negamax(depth - 1, -beta, -alpha, true, pv)
    }

    /// Quiescence search: only looks at captures.
    ///
    /// quiescent search: https://web.archive.org/web/20071027170528/http://www.brucemo.com/compchess/programming/quiescent.htm
    fn quiescent(&mut self, mut alpha: i32, beta: i32) -> i32 {
        alpha = alpha.max(eval::score(&self.board));
        if alpha >= beta {
            return beta;
        }

        let mut info = UndoInfo::default();
        let mut captures = MoveList::default();
        self.gen.gen_captures(&mut captures, &self.board);

        ordering::sort_captures(&mut captures, &self.board);

        for mv in &captures {
            self.board.make_move(mv, &mut info);
            alpha = alpha.max(-self.quiescent(-beta, -alpha));
            self.board.undo_move(mv, &info);

            if alpha >= beta {
                return beta;
            }
        }

        alpha
    }

    /// Null move pruning: skip move and perform a reduced search depth.
    /// If score is still high enough for a beta cutoff, return beta.
    /// This function determines the null move score.
    ///
    /// null move pruning: https://web.archive.org/web/20071031095933/http://www.brucemo.com/compchess/programming/nullmove.htm
    fn null_move(&mut self, depth: usize, alpha: i32, beta: i32) -> i32 {
        if depth >= NULL_REDUCTION {
            let in_check = self.gen.is_in_check(&self.board);
            let is_endgame = eval::is_endgame(&self.board);

            // only perform null move if not in check and not in endgame,
            // to avoid zugzwang positions.
            if !in_check && !is_endgame {
                let mut info = UndoInfo::default();
                self.board.make_move(Move::null(), &mut info);

                // A pv is needed to call negamax, but this result is discarded
                // unless a beta cutoff is caused, so the pv line will never be
                // used. Hence a local pv in this function is created and never read.
                let mut pv = Pv::default();

                // evaluate position after a null move was made.
                // the `null` argument is set to false so that
                // two null moves cannot be made in a row.
                let score = -self.negamax(depth - NULL_REDUCTION, -beta, -beta + 1, false, &mut pv);

                self.board.undo_move(Move::null(), &info);

                // caller must now compare this value to beta to identify
                // the potential cutoff.
                return score;
            }
        }

        // no pruning, assume min bound.
        alpha
    }
}
