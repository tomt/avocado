//! Principal variation implementation.

use crate::game::moves::Move;
use std::fmt;

const MAX_DEPTH: usize = 32;

/// Records the Principal variation.
#[derive(Clone, Copy, Debug)]
pub struct Pv {
    /// Length of the PV.
    len: usize,
    /// List of moves in the PV.
    moves: [Move; MAX_DEPTH],
}

impl fmt::Display for Pv {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        for mv in &self.moves[0..self.len] {
            write!(f, " {mv}")?;
        }

        Ok(())
    }
}
impl Default for Pv {
    fn default() -> Self {
        Pv {
            len: 0,
            moves: [Move::null(); MAX_DEPTH],
        }
    }
}
impl Pv {
    /// Sets this pv to [mv, other..].
    pub fn update(&mut self, mv: Move, other: &Pv) {
        self.moves[0] = mv;
        self.len = other.len + 1;
        self.moves[1..self.len].copy_from_slice(&other.moves[0..other.len]);
    }
    /// Clears the pv.
    pub fn clear(&mut self) {
        self.len = 0;
    }
    /// Gets the length.
    pub fn len(&self) -> usize {
        self.len
    }
    /// Checks whether the Pv is empty.
    pub fn is_empty(&self) -> bool {
        self.len == 0
    }
    /// Gets the first move.
    pub fn first(&self) -> Option<Move> {
        match self.len {
            0 => None,
            _ => Some(self.moves[0]),
        }
    }
}
