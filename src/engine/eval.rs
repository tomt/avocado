//! Contains functions to evaluate a board position.
//!
//! (https://www.chessprogramming.org/Simplified_Evaluation_Function)

use crate::game::{board::Board, color::Color, piece::Piece, square::Square};

/// Determines whether a position has reached endgame.
pub fn is_endgame(board: &Board) -> bool {
    let black_queen = !board.bb(Piece::BlackQueen).is_empty();
    let white_queen = !board.bb(Piece::WhiteQueen).is_empty();

    let black_minor = !board.bb(Piece::BlackBishop).is_empty()
        || !board.bb(Piece::BlackKnight).is_empty()
        || !board.bb(Piece::BlackRook).is_empty();
    let white_minor = !board.bb(Piece::WhiteBishop).is_empty()
        || !board.bb(Piece::WhiteKnight).is_empty()
        || !board.bb(Piece::WhiteRook).is_empty();

    // both sides have no queen.
    //      OR
    // all sides that have a queen have either:
    //  - no additional pieces, or
    //  - a single minor piece.
    !(black_queen && black_minor || white_queen && white_minor)
}

/// Calculates a score for the board position.
pub fn score(board: &Board) -> i32 {
    let mut score = 0;
    let sq_value = match is_endgame(board) {
        true => &SQ_VALUE_ENDGAME,
        false => &SQ_VALUE,
    };

    for sq in Square::iter() {
        if let Some(piece) = board[sq] {
            let sq = usize::from(sq);
            let idx = usize::from(piece) % 6;
            score += match piece.color() {
                Color::White => PIECE_VALUE[idx] + sq_value[idx][sq],
                Color::Black => -PIECE_VALUE[idx] - sq_value[idx][56 ^ sq],
            }
        }
    }

    match board.to_play() {
        Color::White => score,
        Color::Black => -score,
    }
}

/// Bonus values for white pieces on each square.
const SQ_VALUE: [[i32; 64]; 6] = [
    PAWN_MAP,   // 0
    KNIGHT_MAP, // 1
    BISHOP_MAP, // 2
    ROOK_MAP,   // 3
    QUEEN_MAP,  // 4
    KING_MAP,   // 5
];

/// Bonus values for white pieces on each square.
const SQ_VALUE_ENDGAME: [[i32; 64]; 6] = [
    PAWN_MAP,         // 0
    KNIGHT_MAP,       // 1
    BISHOP_MAP,       // 2
    ROOK_MAP,         // 3
    QUEEN_MAP,        // 4
    KING_MAP_ENDGAME, // 5
];

/// Values for each piece.
pub const PIECE_VALUE: [i32; 6] = [
    100,   // White Pawn
    315,   // White Knight
    325,   // White Bishop
    500,   // White Rook
    900,   // White Queen
    20000, // White King
];

/// Bonus values for white pawns on each square.
#[rustfmt::skip]
const PAWN_MAP: [i32; 64] = [
    0,  0,  0,  0,  0,  0,  0,  0,
    50, 50, 50, 50, 50, 50, 50, 50,
    10, 10, 20, 30, 30, 20, 10, 10,
     5,  5, 10, 25, 25, 10,  5,  5,
     0,  0,  0, 20, 20,  0,  0,  0,
     5, -5,-10,  0,  0,-10, -5,  5,
     5, 10, 10,-20,-20, 10, 10,  5,
     0,  0,  0,  0,  0,  0,  0,  0
];

/// Bonus values for white knights on each square.
#[rustfmt::skip]
const KNIGHT_MAP: [i32; 64] = [
    -50,-40,-30,-30,-30,-30,-40,-50,
    -40,-20,  0,  0,  0,  0,-20,-40,
    -30,  0, 10, 15, 15, 10,  0,-30,
    -30,  5, 15, 20, 20, 15,  5,-30,
    -30,  0, 15, 20, 20, 15,  0,-30,
    -30,  5, 10, 15, 15, 10,  5,-30,
    -40,-20,  0,  5,  5,  0,-20,-40,
    -50,-40,-30,-30,-30,-30,-40,-50,
];

/// Bonus values for white bishops on each square.
#[rustfmt::skip]
const BISHOP_MAP: [i32; 64] = [
    -20,-10,-10,-10,-10,-10,-10,-20,
    -10,  0,  0,  0,  0,  0,  0,-10,
    -10,  0,  5, 10, 10,  5,  0,-10,
    -10,  5,  5, 10, 10,  5,  5,-10,
    -10,  0, 10, 10, 10, 10,  0,-10,
    -10, 10, 10, 10, 10, 10, 10,-10,
    -10,  5,  0,  0,  0,  0,  5,-10,
    -20,-10,-10,-10,-10,-10,-10,-20,
];

/// Bonus values for white rooks on each square.
#[rustfmt::skip]
const ROOK_MAP: [i32; 64] = [
    0,  0,  0,  0,  0,  0,  0,  0,
    5, 10, 10, 10, 10, 10, 10,  5,
   -5,  0,  0,  0,  0,  0,  0, -5,
   -5,  0,  0,  0,  0,  0,  0, -5,
   -5,  0,  0,  0,  0,  0,  0, -5,
   -5,  0,  0,  0,  0,  0,  0, -5,
   -5,  0,  0,  0,  0,  0,  0, -5,
    0,  0,  0,  5,  5,  0,  0,  0
];

/// Bonus values for white queens on each square.
#[rustfmt::skip]
const QUEEN_MAP: [i32; 64] = [
    -20,-10,-10, -5, -5,-10,-10,-20,
    -10,  0,  0,  0,  0,  0,  0,-10,
    -10,  0,  5,  5,  5,  5,  0,-10,
     -5,  0,  5,  5,  5,  5,  0, -5,
      0,  0,  5,  5,  5,  5,  0, -5,
    -10,  5,  5,  5,  5,  5,  0,-10,
    -10,  0,  5,  0,  0,  0,  0,-10,
    -20,-10,-10, -5, -5,-10,-10,-20
];

/// Bonus values for white kings on each square.
#[rustfmt::skip]
const KING_MAP: [i32; 64] = [
    -30,-40,-40,-50,-50,-40,-40,-30,
    -30,-40,-40,-50,-50,-40,-40,-30,
    -30,-40,-40,-50,-50,-40,-40,-30,
    -30,-40,-40,-50,-50,-40,-40,-30,
    -20,-30,-30,-40,-40,-30,-30,-20,
    -10,-20,-20,-20,-20,-20,-20,-10,
     20, 20,  0,  0,  0,  0, 20, 20,
     20, 30, 10,  0,  0, 10, 30, 20
];

/// Endgame king table.
#[rustfmt::skip]
const KING_MAP_ENDGAME: [i32; 64] = [
    -50,-40,-30,-20,-20,-30,-40,-50,
    -30,-20,-10,  0,  0,-10,-20,-30,
    -30,-10, 20, 30, 30, 20,-10,-30,
    -30,-10, 30, 40, 40, 30,-10,-30,
    -30,-10, 30, 40, 40, 30,-10,-30,
    -30,-10, 20, 30, 30, 20,-10,-30,
    -30,-30,  0,  0,  0,  0,-30,-30,
    -50,-30,-30,-30,-30,-30,-30,-50
];
