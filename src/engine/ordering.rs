//! Move ordering.

use crate::{
    engine::{eval::PIECE_VALUE, evaluation::POS_INF},
    game::{
        bitboard::BitBoard,
        board::Board,
        color::Color,
        move_gen::{Black, Player, White},
        move_list::MoveList,
        moves::{Move, MoveType},
        piece::Piece,
    },
};

/// Additional score for a move found on a transposition
/// table hit.
const MV_TRANSPOSITION: i32 = 1_000_000;
/// Additional score for first killer move.
const MV_KILLER_1: i32 = 900_000;
/// Additional score for second killer move.
const MV_KILLER_2: i32 = 800_000;
/// Additional score for promoting a piece.
const MV_PROMOTION: i32 = 50;

/// History value weight.
const HISTORY_WEIGHT: i32 = 10;
/// Weighting for victim piece value.
const VICTIM_WEIGHT: i32 = 100;

/// Penalty multiplier (\* piece value) for moving into pawn attack.
const MOVE_INTO_PAWN_ATTACK: i32 = -100;

/// Orders the move, based on:
///
/// 1 000 000 Transposition table move.
///   900 000 First killer move.
///   800 000 Second killer move.
///           History heuristic + MVV/LVA.
pub fn sort(
    moves: &mut MoveList,
    board: &Board,
    tt_move: Option<Move>,
    killers: [Move; 2],
    history: &[[i32; 64]; 12],
) {
    let pawns = board.bb(Piece::pawn(!board.to_play()));
    let pawn_attacks = match board.to_play() {
        Color::White => White::shift_up_left(pawns) | White::shift_up_right(pawns),
        Color::Black => Black::shift_up_left(pawns) | Black::shift_up_right(pawns),
    };

    moves.sort_unstable_by_key(|&mv| score(mv, board, tt_move, pawn_attacks, killers, history))
}

/// Orders captures based on MVV/LVA.
pub fn sort_captures(moves: &mut MoveList, board: &Board) {
    moves.sort_unstable_by_key(|&mv| {
        let attacker = board[mv.start()].unwrap();
        let victim = board[mv.end()].unwrap();
        -mvv_lva(attacker, victim)
    });
}

/// Determines a score for a move, which can be used to
/// order moves.
#[inline]
fn score(
    mv: Move,
    board: &Board,
    tt_move: Option<Move>,
    pawn_attacks: BitBoard,
    killers: [Move; 2],
    history: &[[i32; 64]; 12],
) -> i32 {
    let mut score = POS_INF;

    if tt_move == Some(mv) {
        score -= MV_TRANSPOSITION;
    } else if mv == killers[0] {
        score -= MV_KILLER_1;
    } else if mv == killers[1] {
        score -= MV_KILLER_2;
    } else {
        let start = mv.start();
        let end = mv.end();

        // History heuristic score.
        let attacker = board[start].unwrap();
        score -= HISTORY_WEIGHT * history[usize::from(attacker)][usize::from(end)];

        // First determine whether the move is a capture.
        if let Some(victim) = board[end] {
            score -= mvv_lva(attacker, victim);
        }

        // Discourage moving a piece that is not a pawn to a square that
        // is attacked by an opponent's pawn (this includes pieces that are
        // guarded by a pawn).
        if pawn_attacks.contains(mv.end()) {
            let piece = board[start].unwrap();
            score -= MOVE_INTO_PAWN_ATTACK * PIECE_VALUE[usize::from(piece) % 6];
        }

        // Additional score for promotions.
        if mv.move_type() == MoveType::Promotion {
            let piece = mv.promotion_piece().to_piece(board.to_play());

            score -= MV_PROMOTION + PIECE_VALUE[usize::from(piece) % 6];
        }
    }

    score
}

/// Scores moves based on MVV/LVA.
fn mvv_lva(attacker: Piece, victim: Piece) -> i32 {
    // MVV/LVA: Most Valuable Victim / Least Valuable Attacker.
    let v_attacker = PIECE_VALUE[usize::from(attacker) % 6];
    let v_victim = PIECE_VALUE[usize::from(victim) % 6];

    // maximise victim value, minimise attacker value.
    VICTIM_WEIGHT * v_victim - v_attacker
}
