//! Transposition table implementation.
//!
//! Lock-free: https://www.chessprogramming.org/Shared_Hash_Table#Lockless

use crate::game::{bitboard::BitBoard, moves::Move};

/// Transposition table capacity.
const CAPACITY: usize = 1 << 16;

/// An entry in the transposition table.
#[derive(Debug, Clone)]
pub struct Entry {
    key: BitBoard,
    depth: usize,
    node_type: NodeType,
    best_move: Option<Move>,
    score: i32,
}

impl Default for Entry {
    fn default() -> Self {
        Entry {
            key: BitBoard::default(),
            depth: 0,
            node_type: NodeType::Exact,
            best_move: None,
            score: 0,
        }
    }
}

/// Which kind of node the score value came from.
#[derive(Debug, Clone, Copy)]
pub enum NodeType {
    /// Node had this exact score.
    Exact = 0,
    /// Upper bound on the score.
    Alpha = 1,
    /// Lower bound on the score.
    Beta = 2,
}

/// A transposition table.
#[derive(Debug)]
pub struct Transpositions {
    entries: Vec<Entry>,
}
impl Default for Transpositions {
    fn default() -> Self {
        Self {
            entries: vec![Entry::default(); CAPACITY],
        }
    }
}
impl Transpositions {
    /// Gets the table index from a zobrist hash.
    fn idx(&self, key: BitBoard) -> usize {
        let len = self.entries.len() as u64;
        (u64::from(key) % len) as usize
    }

    /// Gets the proportion of the table that is full, per mill.
    pub fn hashfull(&self) -> u32 {
        let occupied = self
            .entries
            .iter()
            .filter(|e| u64::from(e.key) != 0)
            .count();
        (1_000 * occupied / self.entries.len()) as u32
    }

    /// Gets the optional (suggested move, score) tuple from the table, for a position
    /// with the same zobrist key.
    pub fn lookup(
        &self,
        key: BitBoard,
        depth: usize,
        alpha: i32,
        beta: i32,
    ) -> (Option<Move>, Option<i32>) {
        let entry = &self.entries[self.idx(key)];

        match key == entry.key {
            // collision, different keys.
            false => (None, None),
            // correct keys, ensure that best_move is not a
            // null move, Move(0).
            true => (
                entry.best_move,
                match (entry.depth >= depth, entry.node_type) {
                    (true, NodeType::Exact) => Some(entry.score),
                    (true, NodeType::Alpha) if entry.score <= alpha => Some(alpha),
                    (true, NodeType::Beta) if entry.score >= beta => Some(beta),
                    _ => None,
                },
            ),
        }
    }

    /// Stores hash data for a position.
    pub fn store(
        &mut self,
        key: BitBoard,
        depth: usize,
        node_type: NodeType,
        best_move: Option<Move>,
        score: i32,
    ) {
        let idx = self.idx(key);
        self.entries[idx] = Entry {
            key,
            depth,
            node_type,
            best_move,
            score,
        };
    }
}
