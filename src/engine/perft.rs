//! Perft and perft divide.

use crate::{
    game::{
        board::{Board, UndoInfo},
        move_gen::MoveGen,
        move_list::MoveList,
    },
    uci::response::{Perft, UciResponse},
};
use std::sync::{
    atomic::{AtomicBool, Ordering},
    mpsc::{self, SendError},
};

/// Returns the number of nodes at `depth` from the initial
/// `board` position.
pub fn perft(depth: usize, board: &mut Board, gen: &MoveGen, stop: Option<&AtomicBool>) -> u64 {
    // early exit.
    if let Some(stop) = stop {
        if stop.load(Ordering::Relaxed) {
            return 0;
        }
    }

    let mut move_list = MoveList::default();
    gen.gen_moves(&mut move_list, board);

    if depth <= 1 {
        return move_list.len() as u64;
    }

    let mut nodes = 0;
    let mut info = UndoInfo::default();

    for mv in &move_list {
        board.make_move(mv, &mut info);
        nodes += perft(depth - 1, board, gen, stop);
        board.undo_move(mv, &info);
    }

    nodes
}

/// Perft divide - returns the number of nodes at `depth` from
/// the initial `board` position. Prints a breakdown of the number
/// of nodes from each leaf of the starting position to stdout,
/// along with the total node count.
pub fn perft_divide(
    depth: usize,
    board: &mut Board,
    gen: &MoveGen,
    tx: mpsc::Sender<UciResponse>,
    stop: Option<&AtomicBool>,
) -> Result<u64, SendError<UciResponse>> {
    let mut move_list = MoveList::default();
    gen.gen_moves(&mut move_list, board);

    let mut nodes = 0;
    let mut info = UndoInfo::default();

    for mv in &move_list {
        board.make_move(mv, &mut info);

        let child_nodes = perft(depth - 1, board, gen, stop);

        // early exit.
        if let Some(stop) = stop {
            if stop.load(Ordering::Relaxed) {
                return Ok(0);
            }
        }

        tx.send(UciResponse::Perft(Perft::Divide(mv, child_nodes)))?;

        nodes += child_nodes;
        board.undo_move(mv, &info);
    }

    tx.send(UciResponse::Perft(Perft::NodesSearched(nodes)))?;

    Ok(nodes)
}
