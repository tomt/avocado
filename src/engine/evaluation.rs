//! Scoring type which stores some additional information,
//! such as the number of ply until checkmate.

/// Draw = zero.
pub const DRAW: i32 = 0;
/// Maximum score.
pub const POS_INF: i32 = 1 << 20;
/// Minimum score.
pub const NEG_INF: i32 = -POS_INF;

/// The type of score.
pub enum Evaluation {
    /// Draw by stalemate.
    Stalemate,
    /// Draw by 50 move rule.
    Draw50,
    /// Draw by threefold repetition.
    ThreefoldRepetition,
    /// Draw by low material.
    LowMaterial,
    /// Checkmate in the contained number of ply.
    MateIn(u8),
    /// A static evaluation of a position that is not checkmate.
    Exact(i32),
}

/// A score, evaluates a position. Only 24 bits are used for the
/// actual score, giving a maximum of 2^23 - 1 = 8_388_607, and a
/// minimum of -2^23 = -8_388_608 (one sign bit).
///
/// Positive infinity is defined as:     8_388_607,
/// while negative infinity is defined: -8_388_608.
///
/// The remaining 8 bits are used to store the evaluation:
/// 0000 0000
/// ^^^---------- evaluation type: (stalemate,draw50,threefold,
///                                lowmat,mate,exact).
///                                There are 6 variants so 3 bits
///                                are required.
///    ^-^^^^---- mate ply: 5 bits store the number of ply until a
///                         forced checkmate, the maximum is therefore
///                         32 ply.
///
/// When comparing scores, the 8 most significant bits are masked out,
/// then the value is cast to an i32.
#[repr(transparent)]
#[derive(Default, Clone, Copy, Debug)]
pub struct Score(u32);

impl PartialEq for Score {
    fn eq(&self, other: &Self) -> bool {
        self.score() == other.score()
    }
}
impl Eq for Score {}
impl PartialOrd for Score {
    fn partial_cmp(&self, other: &Self) -> Option<std::cmp::Ordering> {
        Some(self.cmp(other))
    }
}
impl Ord for Score {
    fn cmp(&self, other: &Self) -> std::cmp::Ordering {
        self.score().cmp(&other.score())
    }
}

impl Score {
    fn flags(self) -> u32 {
        let Score(s) = self;
        s >> 24
    }
    fn score(self) -> i32 {
        let Score(s) = self;
        let score = (s & 0x0fff) as i32;
        score - POS_INF
    }

    /// Draw by stalemate.
    pub fn stalemate() -> Self {
        todo!()
    }
    /// Draw by 50 move rule.
    pub fn draw50() -> Self {
        todo!()
    }
    /// Draw by threefold repetition.
    pub fn threefold_repetition() -> Self {
        todo!()
    }
    /// Draw by low material.
    pub fn low_material() -> Self {
        todo!()
    }
    /// Checkmate in the contained number of ply.
    pub fn mate_in(ply: usize) -> Self {
        todo!()
    }
    /// Checkmate in the contained number of ply.
    pub fn exact(score: i32) -> Self {
        debug_assert!((NEG_INF..=POS_INF).contains(&score));
        todo!()
    }

    /// Gets the evaluation of this score.
    pub fn evaluation(self) -> Evaluation {
        todo!()
    }
}
