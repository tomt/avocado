use avocado::uci::Uci;

fn main() {
    Uci::default().run();
}
