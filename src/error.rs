//! The library error module

use std::fmt;

/// An alias of `std::result::Result` with the library `Error` type.
pub type Result<T> = std::result::Result<T, Error>;

/// The library error type.
#[derive(Debug)]
pub enum Error {
    /// Failed to parse promotion.
    PromotionParse,
    /// The String provided to Square::try_from was improperly formatted.
    SquareFormat,
    /// The number of whitespace delimited args provided in the FEN string
    /// was different from expected (6).
    FenArgsLen(usize),
    /// Failed to parse an integer in a FEN string
    FenParseInt(std::num::ParseIntError),
    /// Bad char in FEN castle rights string
    FenCastleBadChar(char),
    /// Unmatched color value in a FEN string (should be "w" | "white" | "b" | "black")
    FenParseColor(String),
    /// Bad char in FEN pieces string
    FenBadChar(char),
    /// Not enough pieces were provided in FEN string
    FenPiecesLen(usize),
    /// An empty FEN string was provided
    FenEmpty,
}

impl fmt::Display for Error {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Error::PromotionParse => write!(f, "Failed to parse promotion"),
            Error::SquareFormat => write!(f, "Invalid square"),
            Error::FenArgsLen(len) => write!(f, "Wrong number of FEN tokens: {len} (expect 6)"),
            Error::FenParseInt(e) => write!(f, "Failed to parse integer in FEN string: {e}"),
            Error::FenCastleBadChar(ch) => {
                write!(f, "Expected only [QqKk-] in FEN castle string, found: {ch}")
            }
            Error::FenParseColor(s) => write!(f, "Invalid color in FEN: {s}"),
            Error::FenBadChar(ch) => write!(f, "Invalid char in FEN pieces: {ch}"),
            Error::FenPiecesLen(len) => write!(
                f,
                "Not enough pieces or squares were provided in FEN string, found: {len}"
            ),
            Error::FenEmpty => write!(f, "FEN was empty"),
        }
    }
}
impl std::error::Error for Error {}
